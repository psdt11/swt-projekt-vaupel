import list.CUI;
import queue.InterfaceQueue;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {

        Scanner input = new Scanner(System.in);
        int choice;
        boolean run = true;
        InterfaceQueue iq;
        CUI cui;

        while (run) {
            System.out.println("Welcome to the Algorithm-Helper.\n");
            System.out.println("Which datastructure do you like to visualize?");
            System.out.println("1: Queue");
            System.out.println("2: List");
            System.out.println("3: Exit the programm");

            choice = getValidInput();

            /*do {
                try {
                    choice = input.nextInt();
                    break;
                } catch (Exception e) {
                    System.out.println(input.next() + " Was not an Integer try again");
                }

            } while (input.hasNext());*/

            switch (choice) {
                case 1 -> {
                    iq = new InterfaceQueue();
                    iq.start();
                }

                case 2 -> {
                    cui = new CUI();
                    CUI.listInterface();
                }

                case 3 -> run = false;

            }
        }
    }

    private static int getValidInput() {
        Scanner input = new Scanner(System.in);
        int value = 0;
        do {
            try {
                value = input.nextInt();
                break;
            } catch (Exception e) {
                System.out.println(input.next() + " Was not an Integer try again");
            }
        } while (input.hasNext());

        return value;
    }
}
