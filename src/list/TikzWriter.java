package list;

public class TikzWriter {

    /**
     * Header of the document
     * Contains several macros
     *
     * @return the header of the document
     */
    static String printStartOfDocument() {
        return ("\\newcommand{\\listenelementMarked}[3]{\n" +
                "\\fill[red!40!white](0,0)--(0,1)--(4,1)--(4,0)--(0,0);\n" +
                "\\draw[red](1,1)--(1,0);\n" +
                "\\draw[red](3,1)--(3,0);\n" +
                "\\% Vorgänger\n" +
                "\\draw(0.5,0.5)node[]{#1};\n" +
                "\\% Wert\n" +
                "\\draw(2.0,0.5)node[]{#2};\n" +
                "\\% Nachfolger\n" +
                "\\draw(3.5,0.5)node[]{#3};\n" +
                "}\n" +
                "\\newcommand{\\listenelementMarkedNext}[3]{\n" +
                "\\fill[green!30!white](0,0)--(0,1)--(4,1)--(4,0)--(0,0);\n" +
                "\\draw[green](1,1)--(1,0);\n" +
                "\\draw[green](3,1)--(3,0);\n" +
                "% Vorgänger\n" +
                "\\draw(0.5,0.5)node[]{#1};\n" +
                "% Wert\n" +
                "\\draw(2.0,0.5)node[]{#2};\n" +
                "% Nachfolger\n" +
                "\\draw(3.5,0.5)node[]{#3};\n" +
                "}\n" +
                "\\newcommand{\\listenelement}[3]{\n" +
                "\\draw(0,0)--(0,1)--(4,1)--(4,0)--(0,0);\n" +
                "\\draw(1,1)--(1,0);\n" +
                "\\draw(3,1)--(3,0);\n" +
                "% Vorgänger\n" +
                "\\draw(0.5,0.5)node[]{#1};\n" +
                "% Wert\n" +
                "\\draw(2.0,0.5)node[]{#2};\n" +
                "% Nachfolger\n" +
                "\\draw(3.5,0.5)node[]{#3};\n" +
                "}\n" +
                "\\begin{tikzpicture}" + "\n");
    }

    /**
     * Footer of the tikz document
     *
     * @return the footer to conclude the document
     */
    static String printEndOfDocument() {
        return "\\end{tikzpicture}";
    }

    /**
     * Used to generate tikz code displaying a plain line
     * called in printListToLatex function in the ListPrinter class
     *
     * @param start  sets starting point of line
     * @param height sets height of line
     * @param end    sets end of line
     * @param color  sets color of line
     * @return the generated tikz code
     */
    static String printLine(double start, double height, double end, String color) {
        return "\\draw[" + color + " (" + start + "," + (height + 0.5) + ") -- (" + end + "," + (height + 0.5) + ");\n";
    }

    /**
     * Used to generate tikz code displaying a line with an arrowhead
     * called in printListToLatex function in the ListPrinter class
     *
     * @param start  sets starting point of line
     * @param height sets height of line
     * @param end    sets end of line
     * @param color  sets color of line
     * @return the generated tikz code
     */
    static String printLineWithArrow(double start, double height, double end, String color) {
        return "\\draw[" + color + "->] (" + start + "," + (height + 0.5) + ") -- (" + end + "," + (height + 0.5) + ");\n";
    }

    /**
     * Used to generate tikz code displaying a textbox
     * similar to the below function difference being
     * this textbox will be placed in the middle horizontally
     *
     * @param text   that will be printed
     * @param height sets the height at which the text is to be printed
     * @return the generated tikz code
     */
    static String printText(String text, double height) {
        return "\\node[draw] at (8," + height + ") {" + text + "};\n";
    }

    /**
     * Used to generate tikz code displaying a textbox
     *
     * @param text   that will be printed
     * @param height sets the height at which the text is to be printed
     * @param posX   sets the x coordinate at which the text is to be printed
     * @return the generated tikz code
     */
    static String printText(String text, double height, double posX) {
        return "\\node[draw] at (" + posX + ", " + height + ") {" + text + "};\n";
    }

    /**
     * used to generate tikz code displaying three dots
     * this function is called in the printListToLatex function
     * which may print only part of the list and these dots represent
     * that not the whole list has been printed
     *
     * @param height sets the height at which the dots are to be printed
     * @param posX   sets the x coordinate at which the dots are to be printed
     * @return the generated tikz code
     */
    static String printDots(double height, double posX) {
        return "\\fill(" + posX + "," + height + ") circle(0.08);\n" +
                "\\fill(" + (posX - 0.5) + ", " + height + ") circle(0.08);\n" +
                "\\fill(" + (posX - 1) + ", " + height + ") circle(0.08);\n";
    }

    /**
     * Used to generate tikz code displaying lines with arrowheads that connect nodes
     * from two different rows, called in ListPrinter and Sorter classes
     *
     * @param height sets the height at which the line is to be printed
     * @param left   determines in which direction the arrowhead points to
     * @return the generated tikz code
     */
    static String printCrossLine(double height, boolean left) {
        if (left) {
            return "\\draw(16," + (height + 0.5) + ")-|(17," + (height - 1) + ");\n" +
                    "\\draw(17," + (height - 1) + ")--(-1.5," + (height - 1) + ");\n" +
                    "\\draw[->](-1.5," + (height - 1) + ")|-(0," + (height - 2.5) + ");\n";
        } else {
            return "\\draw[<-](16," + (height + 0.5) + ")-|(17.5," + (height - 1) + ");\n" +
                    "\\draw(17.5," + (height - 1) + ")--(-1," + (height - 1) + ");\n" +
                    "\\draw(-1," + (height - 1) + ")|-(0," + (height - 2.5) + ");\n";
        }
    }

    /**
     * Used to generate tikz code displaying a node(ListElement)
     * code checks for various different node states
     * start/end nodes and marked/unmarked nodes
     *
     * @param counterX        used for alignment
     * @param spaceInBetweenX used for alignment
     * @param counterY        used for alignment
     * @param spaceInBetweenY used for alignment
     * @param node            that will be printed has the metainformation to check for states
     * @param start           start of list needed to show if printed node is start node
     * @param end             end of list needed to show if printed node is end node
     * @return the generated tikz code
     */
    static String printNode(int counterX, double spaceInBetweenX, double counterY, double spaceInBetweenY, ListElement node, ListElement start, ListElement end) {


        if (node.value == -1 && node == start) {
            return "\\begin{scope}\n"
                    + "[xshift = " + counterX * spaceInBetweenX + "cm, yshift = " + counterY * spaceInBetweenY + "cm]\\listenelement{P}{Start}{N}\n" +
                    "\\end{scope}\n";
        } else if (node.value == -1 && node == end) {
            return "\\begin{scope}\n"
                    + "[xshift = " + counterX * spaceInBetweenX + "cm, yshift = " + counterY * spaceInBetweenY + "cm]\\listenelement{P}{End}{N}\n" +
                    "\\end{scope}\n";
        } else if (node.marked) {
            return "\\begin{scope}\n"
                    + "[xshift = " + counterX * spaceInBetweenX + "cm, yshift = " + counterY * spaceInBetweenY + "cm]\\listenelementMarked{P}{" + node.value + "}{N}\n" +
                    "\\end{scope}\n";
        } else if (node.prev.marked) {
            return "\\begin{scope}\n"
                    + "[xshift = " + counterX * spaceInBetweenX + "cm, yshift = " + counterY * spaceInBetweenY + "cm]\\listenelementMarkedNext{P}{" + node.value + "}{N}\n" +
                    "\\end{scope}\n";
        } else {
            return "\\begin{scope}\n"
                    + "[xshift = " + counterX * spaceInBetweenX + "cm, yshift = " + counterY * spaceInBetweenY + "cm]\\listenelement{P}{" + node.value + "}{N}\n" +
                    "\\end{scope}\n";
        }

    }

    /**
     * Used to generate tikz code that displays a swap of two nodes and their respective pointers
     * called in the sort function in class Sorter
     *
     * @param startOfList start of List needed to show start node in case swap is happening near start of the list
     * @param endOfList   end of List needed to show end node in case swap is happening near end of the list
     * @param a           Node a will be swapped with Node b
     * @param b           Node b will be swapped with Node a
     * @param offsetY     offset in relation to other construct in order for overlapping not to happen
     * @return the generated tikz code
     */
    static String printSwap(ListElement startOfList, ListElement endOfList, ListElement a, ListElement b, double offsetY) {
        return printNode(-1, 1, offsetY, -3, a.prev, startOfList, endOfList) +
                printNode(4, 1, offsetY, -3, a, startOfList, endOfList) +
                printNode(9, 1, offsetY, -3, b, startOfList, endOfList) +
                printNode(14, 1, offsetY, -3, b.next, startOfList, endOfList) +
                printLineWithArrow(3, 0.25 - offsetY * 3, 4, "") +
                printLineWithArrow(4, -0.25 - offsetY * 3, 3, "") +
                printLineWithArrow(13, 0.25 - offsetY * 3, 14, "") +
                printLineWithArrow(14, -0.25 - offsetY * 3, 13, "") +
                "\\draw[thick,<->] (6," + (1 - offsetY * 3) + ") ..  controls (8," + (3 - offsetY * 3) + ") and (9," + (3 - offsetY * 3) + ") .. (11," + (1 - offsetY * 3) + ");\n";
    }
}







