package list;

public class testList {

    public static void main(String[] args) {


        DoubleLinkedList list = new DoubleLinkedList();
        assert list.length() == 0;

        list.addRandomValues(5);
        assert list.length() == 5;

        list.addRandomValues(-12);
        assert list.length() == 5;

        list.deleteAllElements();
        assert list.length() == 0;

        list.prepend(100);
        assert list.length() == 1;

        list.append(42);
        assert list.end.prev.value == 42;

        list.deleteAllElements();
        list.addRandomValues(1);
        assert list.start.next.value <= 100 && list.start.next.value >= 10;

        list.deleteAllElements();
        try {
            list.deletePos(10);
        } catch (Exception e) {
            System.out.println("Exception caught all is good");
        }
        try {
            list.insertAt(-10, 10);
        } catch (Exception e) {
            System.out.println("Second Exception caught all is good");
        }

        System.out.println("All assertions were correct");

    }
}
