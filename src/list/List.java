package list;

public interface List {

    void prepend(int value);                //add of new Element with value to the end of the List

    void append(int value);                 // add of new Element with value to the start of the List

    int length();                           //return the length of the List

    void deletePos(int position);           //deletes Node at position

    void insertAt(int position, int value); //add the Element at position into the List

    void printList();                       //print the whole List
}
