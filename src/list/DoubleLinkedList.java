package list;

public class DoubleLinkedList implements List {
    public ListElement start;
    public ListElement end;

    /**
     * Constuctor that initializes the List
     */
    public DoubleLinkedList() {                   //create List
        start = new ListElement(-1);
        end = new ListElement(-1);
        start.prev = null;
        start.next = end;
        end.prev = start;
        end.next = null;
    }

    /**
     * Insert value directly after Start node
     *
     * @param value that will be inserted
     */
    public void prepend(int value) { //add of new Element with value to the end of the List
        ListElement newNode;
        newNode = new ListElement(value);
        newNode.prev = start;
        newNode.next = start.next;
        start.next.prev = newNode;
        start.next = newNode;
    }

    public void append(int value) { // add of new Element with value to the start of the List
        ListElement newNode = new ListElement(value);

        ListElement traverse;
        traverse = start;

        while (traverse.next != end) {
            traverse = traverse.next;
        }

        end.prev.next = newNode;
        newNode.next = end;

        newNode.prev = end.prev;
        end.prev = newNode;
    }

    /**
     * Traverses entire List and counts how many steps it took to get to end of List
     * @return the result of this process
     */
    public int length() { //return the length of the List
        int counter = 0;
        ListElement traverse;
        traverse = start;

        while (traverse.next != end) {
            traverse = traverse.next;
            counter++;
        }
        return counter;
    }

    public void printList() { //print the whole List
        ListElement traverse;
        traverse = start;

        ListElement traverse2;
        traverse2 = start;
        int posCounter = 1;

        System.out.print("Pos" + "\t\t");
        while (traverse2.next != end) {
            traverse2 = traverse2.next;
            System.out.print(posCounter + "\t");
            posCounter++;
        }

        System.out.println();

        System.out.print("Start" + "\t");

        while (traverse.next != end) {
            traverse = traverse.next;
            System.out.print(traverse.value + "\t");
        }

        System.out.println("End\n");

    }

    public void deletePos(int position) { //deletes Node at position
        if (position > length() || position <= 0)
            throw new IndexOutOfBoundsException();
        int counter = 0;

        ListElement traverse;
        traverse = start;

        while (traverse.next != end) {
            if (counter == position) {
                ListElement next = traverse.next;
                ListElement prev = traverse.prev;

                next.prev = prev;
                prev.next = next;
                return;
            }
            traverse = traverse.next;
            counter++;
        }
    }

    /**
     * Traverses List and counts each step until steps reach
     * the param position and this points the param value is inserted
     * and the pointers are set accordingly
     * @param position is the position where we will place a new node
     * @param value is the value of the newly placed node
     */
    public void insertAt(int position, int value) { //add the Element at position into the List
        if (position > length() || position <= 0)
            throw new IndexOutOfBoundsException();

        ListElement newNode = new ListElement(value);
        int counter = 1;


        ListElement traverse;
        traverse = start;

        while (traverse.next != end) {
            if (counter == position) {
                ListElement restOfList = traverse.next;
                restOfList.prev = newNode;

                newNode.prev = traverse;
                newNode.next = restOfList;

                traverse.next = newNode;
            }
            traverse = traverse.next;
            counter++;
        }
    }

    public void addRandomValues(int n, int upper, int lower) {
        for (int i = 0; i < n; i++) {
            append(lower + (((int) (Math.random() * upper))));
        }
    }

    /**
     * Uses the append function with a random value between 10 and 100 as parameter
     * useful for quickly filling a list
     * @param n is the number of times new nodes will be appended
     */
    public void addRandomValues(int n) { //adds n random Values to the start of the List
        for (int i = 0; i < n; i++) {
            append(10 + (((int) (Math.random() * 90))));
        }
    }

    public void deleteAllElements() {
        start.next = end;
        end.prev = start;
    }
}



