package list;

import java.io.FileWriter;
import java.io.IOException;

public class Sorter {

    /**
     * Helper function for the sort function
     * swaps to nodes and their respective pointers
     *
     * @param a Node a will be swapped with b
     * @param b Node b will be swapped with a
     */
    public static void swap(ListElement a, ListElement b) {

        ListElement first = a.prev;
        ListElement last = b.next;

        first.next = b;
        last.prev = a;

        a.next = last;
        b.prev = first;

        a.prev = b;
        b.next = a;
    }

    /**
     * Sorts the list with a bubble sort algorithm
     * with the right parameters also prints the process
     * into a tikz document
     *
     * @param list          that will be sorted
     * @param print         if true the process will be printed into a tikz document
     * @param detailed      if true the printing process will be more detailed
     * @param showSwaps     if true the printing process also shows swaps
     * @param printSeparate if true every list state will be printed separately
     * @param fileName      file will be created with fileName as its name
     * @throws IOException  may be throws in called functions
     */
    public static void sort(DoubleLinkedList list, boolean print, boolean detailed, boolean showSwaps, boolean printSeparate, String fileName) throws IOException {

        boolean swapped = true;
        int maxSize = 30;
        int printCounter = 0;
        int swapOffset = 0;
        int spaceOffset = 0;
        int runCounter = 0;
        int swapCounter = 0;
        int numberOfPrintsOnPage;
        String fileNameChangable = fileName;
        int fileCounter = 2;
        FileWriter myWriter;

        if (printSeparate)
            numberOfPrintsOnPage = 1;
        else
            numberOfPrintsOnPage = maxSize - list.length();

        if (!printSeparate && maxSize - list.length() <= 0) {
            System.out.println("List is too long to print in one File, try printing the List into seperate Files");
            return;
        }

        if (list.length() < 2) {
            System.out.println("List is already Sorted");
            if (print) ListPrinter.printListToLatex(list.start, list.end, fileName);
            return;
        }

        if (print) {
            myWriter = new FileWriter(fileNameChangable);
            myWriter.write(TikzWriter.printStartOfDocument());
            myWriter.write(TikzWriter.printText("Sorting List with " + list.length() + " nodes", 3, 8));
            myWriter.write(TikzWriter.printText("Starting Algorithm, Red Node = current, Green Node = next", 2, 8));
            myWriter.close();
            ListPrinter.printListToLatex(list.start, list.end, fileNameChangable, 0, true);
            printCounter++;
        }

        while (swapped) {
            ListElement traverse = list.start;
            swapped = false;
            runCounter++;

            while (traverse.next.next != list.end) {

                traverse = traverse.next;

                if (print) {
                    if (printCounter >= numberOfPrintsOnPage) {
                        myWriter = new FileWriter(fileNameChangable, true);
                        myWriter.write(TikzWriter.printEndOfDocument());
                        myWriter.close();

                        String[] tmp = fileName.split("\\.", 2);
                        fileNameChangable = tmp[0] + "part" + fileCounter + ".tikz";
                        fileCounter++;
                        myWriter = new FileWriter(fileNameChangable);
                        myWriter.write(TikzWriter.printStartOfDocument());
                        myWriter.close();
                        swapOffset = 0;
                        spaceOffset = 0;
                        printCounter = 0;
                    }

                    traverse.marked = true;
                    ListPrinter.printListToLatex(list.start, list.end, fileNameChangable, ListPrinter.calculateOffset(list, printCounter, swapOffset + spaceOffset), true);

                    if (detailed && traverse.value > traverse.next.value) {
                        myWriter = new FileWriter(fileNameChangable, true);
                        myWriter.write(TikzWriter.printText("Red greater than  Green, therefore Swap is needed", 3 - ListPrinter.calculateOffset(list, printCounter, swapOffset + spaceOffset)));
                        myWriter.close();
                    } else if (detailed) {
                        myWriter = new FileWriter(fileNameChangable, true);
                        myWriter.write(TikzWriter.printText("Red less or equal than/to Green, therefore no Swap is needed", 3 - ListPrinter.calculateOffset(list, printCounter, swapOffset + spaceOffset)));
                        myWriter.close();
                    }

                    if (detailed) {
                        myWriter = new FileWriter(fileNameChangable, true);
                        myWriter.write(TikzWriter.printText("Run : " + runCounter, 3 - ListPrinter.calculateOffset(list, printCounter, swapOffset + spaceOffset), 0));
                        myWriter.write(TikzWriter.printText("Swaps : " + swapCounter, 2 - ListPrinter.calculateOffset(list, printCounter, swapOffset + spaceOffset), 0));
                        myWriter.close();
                    }
                    if (detailed && swapped && traverse.next.next == list.end) {
                        myWriter = new FileWriter(fileNameChangable, true);
                        myWriter.write(TikzWriter.printText("End of List reached but Swap was needed in this run, restarting algorithm", 2 - ListPrinter.calculateOffset(list, printCounter, swapOffset + spaceOffset)));
                        myWriter.close();
                    }
                    if (showSwaps && traverse.value > traverse.next.value) {
                        myWriter = new FileWriter(fileNameChangable, true);
                        myWriter.write((TikzWriter.printSwap(list.start, list.end, traverse, traverse.next, ListPrinter.calculateOffset(list, printCounter + 1, swapOffset + spaceOffset) / 3)));
                        myWriter.close();
                        swapOffset += 5;
                    }
                    traverse.marked = false;
                    printCounter++;
                    spaceOffset += 2;
                }

                if (traverse.value > traverse.next.value) {
                    swap(traverse, traverse.next);
                    traverse = traverse.prev;
                    swapped = true;
                    swapCounter++;
                }
            }
        }

        if (detailed) {
            myWriter = new FileWriter(fileNameChangable, true);
            myWriter.write(TikzWriter.printText("End of List reached but Swap was not needed in this run, List is sorted", 2 - ListPrinter.calculateOffset(list, printCounter - 1, swapOffset + spaceOffset - 2)));
            myWriter.write(TikzWriter.printEndOfDocument());
            myWriter.close();
        } else {
            myWriter = new FileWriter(fileNameChangable, true);
            myWriter.write(TikzWriter.printEndOfDocument());
            myWriter.close();
        }
    }

    /**
     * Helper function to call the function with the parameters set to just sort the list
     *
     * @param list     list to be sorted
     * @param fileName file will be created with fileName as its name
     * @throws IOException may be throws in called function
     */
    public static void sort(DoubleLinkedList list, String fileName) throws IOException {
        sort(list, false, false, false, false, fileName);
    }
}
