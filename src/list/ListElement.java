package list;

public class ListElement {
    ListElement prev;
    ListElement next;
    int value;
    boolean marked;

    ListElement(int value) {
        this.next = null;
        this.prev = null;
        this.value = value;
        this.marked =false;
    }
}

