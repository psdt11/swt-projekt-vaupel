package list;

import java.io.FileWriter;
import java.io.IOException;

public class ListPrinter {

    /**
     * Dynamically calculates the offset needed for another object to be printed based on parameters
     *
     * @param list    needed to get the length of the list and adjust accordingly
     * @param counter used to determine which object is printed on the document
     *                the 3rd document will call the function with counter=3
     * @param offset  used for offsetting the distance if necessary
     * @return the calculated Offset
     */
    public static double calculateOffset(DoubleLinkedList list, int counter, double offset) {
        return counter * ((Math.ceil(list.length() + 2) / 3) * 4) + offset + 2;
    }

    /**
     * Standard version of this function, used to print the whole List into a document
     * see overloaded functions fore more detail
     *
     * @param startOfList start of the list where printing will start
     * @param endOfList   end of the list where printing will stop
     * @param fileName    file will be created with fileName as its name
     * @param offsetY     used to offSet the construct in order for overlapping not to happen
     * @param override    used to check if header and footer of document is needed
     * @throws IOException may be throws in called functions
     */
    public static void printListToLatex(ListElement startOfList, ListElement endOfList, String fileName, double offsetY, boolean override) throws IOException { //writes all Elements into Tikz Code

        FileWriter myWriter;
        myWriter = new FileWriter(fileName, override);

        ListElement traverse;
        traverse = startOfList;
        int numberOfNodesInARow = 3;
        int counterX = 0;
        int counterY = 0;
        double spaceInBetweenX = 6;
        double spaceInBetweenY = -3;

        //Draw Start Node
        if (!override)
            myWriter.write(TikzWriter.printStartOfDocument());


        while (traverse != endOfList) {                 //Draw ListElement


            myWriter.write(TikzWriter.printNode(counterX, spaceInBetweenX, counterY - (offsetY / spaceInBetweenY), spaceInBetweenY, traverse, startOfList, endOfList));

            if (counterX == 0) {        //Draw Lines
                myWriter.write(TikzWriter.printLineWithArrow(4, (counterY * spaceInBetweenY + 0.25 - offsetY), 6, ""));
                myWriter.write(TikzWriter.printLineWithArrow(6, (counterY * spaceInBetweenY - 0.25 - offsetY), 4, ""));
            } else if (counterX == 1) {
                myWriter.write(TikzWriter.printLineWithArrow(10, (counterY * spaceInBetweenY + 0.25 - offsetY), 12, ""));
                myWriter.write(TikzWriter.printLineWithArrow(12, (counterY * spaceInBetweenY - 0.25 - offsetY), 10, ""));
            }

            traverse = traverse.next;
            counterX++;

            if (counterX == numberOfNodesInARow) {      //Draw Crossline
                myWriter.write(TikzWriter.printCrossLine(counterY * spaceInBetweenY + 0.25 - offsetY, true));
                myWriter.write(TikzWriter.printCrossLine(counterY * spaceInBetweenY - 0.25 - offsetY, false));
                counterX = 0;
                counterY++;
            }
        }

        myWriter.write(TikzWriter.printNode(counterX, spaceInBetweenX, counterY - (offsetY / spaceInBetweenY), spaceInBetweenY, traverse, startOfList, endOfList));

        if (!override)
            myWriter.write(TikzWriter.printEndOfDocument());


        myWriter.close();
    }

    /**
     * Helper function in order to call function more easily with standard parameters
     *
     * @param startOfList start of the List
     * @param endOfList   end of the List
     * @param fileName    file will be created with fileName as its name
     * @throws IOException may be thrown in called functions
     */
    public static void printListToLatex(ListElement startOfList, ListElement endOfList, String fileName) throws IOException {
        printListToLatex(startOfList, endOfList, fileName, 0, false);
    }

    /**
     * Overloaded version of printListToLatex called when user only wants part of the list
     * to be printed in the document, this is accomplished by traversing the list from the front
     * and the back until parameters start and end have been reached the standard version of this function
     * is then called with the new start and end nodes. If the start and end parameters are not the same
     * as the list´s start and end, dots will be printed to represent that part of the list is missing
     * this method uses a lot of methods that print constructs for more information read TikzWriter class docu
     *
     * @param start    specifies where we want to start printing the list
     * @param end      specifies where we want to stop printing the list
     * @param list     is the list used to print the constructs
     * @param fileName the document that will be created will have the fileName as its name
     * @throws IllegalArgumentException is thrown if start and end are not in bounds
     * @throws IOException              may be thrown in one of the called functions
     */
    public static void printListToLatex(int start, int end, DoubleLinkedList list, String fileName) throws IllegalArgumentException, IOException {
        if (start > list.length() || start < 0 || end > list.length() + 1 || end < 0 || start > end)
            throw new IllegalArgumentException();

        ListElement traverseToStart = list.start;
        ListElement traverseToEnd = list.end;
        int counter = 0;
        FileWriter myWriter = new FileWriter(fileName);
        int numberOfNodesPrinted;
        double height;

        while (counter < start) {
            traverseToStart = traverseToStart.next;
            counter++;
        }
        counter = 0;
        while (counter + end <= list.length()) {
            traverseToEnd = traverseToEnd.prev;
            counter++;
        }


        myWriter.write(TikzWriter.printStartOfDocument());

        numberOfNodesPrinted = end - start;
        height = 0.5 - 3 * (numberOfNodesPrinted / 3);

        if (start > 0)
            myWriter.write(TikzWriter.printDots(0.5, -0.5));

        if (end < list.length() + 1) {
            switch ((numberOfNodesPrinted + 1) % 3) {
                case 0 -> myWriter.write(TikzWriter.printDots(height, 17.5));
                case 1 -> myWriter.write(TikzWriter.printDots(height, 5.5));
                case 2 -> myWriter.write(TikzWriter.printDots(height, 11.5));
            }
        }
        myWriter.close();

        printListToLatex(traverseToStart, traverseToEnd, fileName, 0, true);
        myWriter = new FileWriter(fileName, true);
        myWriter.write(TikzWriter.printEndOfDocument());
        myWriter.close();
    }

}
