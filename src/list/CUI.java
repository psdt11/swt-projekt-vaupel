package list;

import java.io.IOException;
import java.util.Scanner;

import static java.lang.Thread.sleep;

public class CUI {

    /**
     * User Interface for the list
     *
     * @throws IOException          may be thrown in called functions
     * @throws InterruptedException may be thrown in called functions
     */
    public static void listInterface() throws IOException, InterruptedException {


        DoubleLinkedList myList = new DoubleLinkedList();
        Scanner input = new Scanner(System.in);
        int choice = 0;
        final int maxSizeOfList = 50;
        String fileName = "output.tikz";
        boolean run = true;
        boolean alwaysPrintChanges = false;
        boolean printSeparate = false;

        while (run) {

            System.out.println("Press the desired Number for the Function to run");
            System.out.println("1 For Printing out the List to Console");
            System.out.println("2 For Adding Element to front of the List");
            System.out.println("3 For Adding Element to back of the List");
            System.out.println("4 For Adding n random Elements to back of the List");
            System.out.println("5 For Adding n random Elements with desired range of values to back of the List");
            System.out.println("6 For Inserting Element at desired Place");
            System.out.println("7 For Deleting Element at desired Place");
            System.out.println("8 For Printing out the Length of the List");
            System.out.println("9 For deleting all Elements from the List");
            System.out.println("10 For sorting the List");
            System.out.println("11 For Always Printing changes to the List to Latex");
            System.out.println("12 For setting the Filename");
            System.out.println("13 For Printing out the List to Latex");
            System.out.println("14 For Printing out the List to Latex in a specified range");
            System.out.println("15 For printing the Output into separate Files");
            System.out.println("16 For sorting the List and printing the sorting process");
            System.out.println("Any other key to Exit the Program\n");


            choice = getValidInput();

            switch (choice) {
                case 1 -> myList.printList();

                case 2 -> {
                    System.out.println("Enter Value :");
                    int value = getValidInput();

                    if (myList.length() + 1 > maxSizeOfList)
                        System.out.println("The Input would make the List too big");
                    else
                        myList.prepend(value);

                    if (alwaysPrintChanges)
                        ListPrinter.printListToLatex(myList.start, myList.end, fileName);
                }
                case 3 -> {
                    System.out.println("Enter Value :");
                    int value = getValidInput();

                    if (myList.length() + 1 > maxSizeOfList)
                        System.out.println("The Input would make the List too big");
                    else
                        myList.append(value);

                    if (alwaysPrintChanges)
                        ListPrinter.printListToLatex(myList.start, myList.end, fileName);
                }
                case 4 -> {
                    System.out.println("Enter Number of Nodes to be created :");

                    int value = getValidInput();

                    if (value + myList.length() > maxSizeOfList)
                        System.out.println("The Input would make the List too big");
                    else
                        myList.addRandomValues(value);

                    if (alwaysPrintChanges)
                        ListPrinter.printListToLatex(myList.start, myList.end, fileName);
                }
                case 5 -> {
                    System.out.println("Enter Number of Nodes to be created :");
                    int value = getValidInput();

                    System.out.println("Enter upper Limit :");
                    int upper = getValidInput();

                    System.out.println("Enter lower Limit :");
                    int lower = getValidInput();

                    if (value + myList.length() > maxSizeOfList)
                        System.out.println("The Input would make the List too big");
                    else
                        myList.addRandomValues(value, upper, lower);

                    if (alwaysPrintChanges)
                        ListPrinter.printListToLatex(myList.start, myList.end, fileName);
                }
                case 6 -> {
                    System.out.println("Enter Position(1-" + myList.length() + ") :");
                    int pos = getValidInput();

                    System.out.println("Enter Value :");
                    int value = getValidInput();

                    try {
                        myList.insertAt(pos, value);
                    } catch (Exception e) {
                        System.out.println("Position larger or smaller than List\n");
                    }

                    if (alwaysPrintChanges)
                        ListPrinter.printListToLatex(myList.start, myList.end, fileName);
                }
                case 7 -> {
                    System.out.println("Enter Position you want to delete(1-" + myList.length() + ")");
                    int pos = getValidInput();

                    try {
                        myList.deletePos(pos);
                    } catch (Exception e) {
                        System.out.println("Position larger or smaller than List\n");
                    }

                    if (alwaysPrintChanges)
                        ListPrinter.printListToLatex(myList.start, myList.end, fileName);
                }
                case 8 -> System.out.println("Length of List is: " + myList.length());
                case 9 -> {
                    myList.deleteAllElements();
                    System.out.println("All Elements have been deleted");
                }
                case 10 -> {
                    Sorter.sort(myList, fileName);
                    if (alwaysPrintChanges)
                        ListPrinter.printListToLatex(myList.start, myList.end, fileName);
                }
                case 11 -> {
                    alwaysPrintChanges = !alwaysPrintChanges;
                    System.out.println("Your Choice has been saved\n");
                }
                case 12 -> {
                    System.out.println("Enter Filename :");
                    fileName = input.next();
                    fileName = fileName + ".tikz";
                }

                case 13 -> ListPrinter.printListToLatex(myList.start, myList.end, fileName);

                case 14 -> {
                    System.out.println("Enter Start(0-" + (myList.length() + 1) + ") :");
                    int start = getValidInput();

                    System.out.println("Enter End(0-" + (myList.length() + 1) + ") :");
                    int end = getValidInput();

                    try {
                        ListPrinter.printListToLatex(start, end, myList, fileName);
                    } catch (Exception e) {
                        System.out.println("Start and end must be in bounds");
                    }
                }
                case 15 -> {
                    printSeparate = !printSeparate;
                    System.out.println("Your Choice has been saved\n");
                }

                case 16 -> {
                    System.out.println("If you created a long list(length > 5) or the list needs a lot of Swaps\n" +
                            "the program might need to create the Output into more than one file.\n");
                    System.out.println("1 For A Simple Output");
                    System.out.println("2 For A detailed Output");
                    System.out.println("3 to print the Swaps in the Output");
                    System.out.println("4 For A detailed Output with Swaps");

                    int sortChoice = getValidInput();

                    switch (sortChoice) {
                        case 1 -> Sorter.sort(myList, true, false, false, printSeparate, fileName);
                        case 2 -> Sorter.sort(myList, true, true, false, printSeparate, fileName);
                        case 3 -> Sorter.sort(myList, true, false, true, printSeparate, fileName);
                        case 4 -> Sorter.sort(myList, true, true, true, printSeparate, fileName);
                        default -> System.out.println("Please input a valid choice");
                    }
                }

                default -> run = false;
            }
            sleep(2000);
        }

    }

    public static int getValidInput() {
        Scanner input = new Scanner(System.in);
        int value = 0;
        do {
            try {
                value = input.nextInt();
                break;
            } catch (Exception e) {
                System.out.println(input.next() + " Was not an Integer try again");
            }
        } while (input.hasNext());

        return value;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        listInterface();
    }
}

