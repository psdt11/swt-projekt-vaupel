package queue;

/**
 * Implementiert das Queue Interface, als Priotity Queue wobei die einzelnen 
 * Elemente nicht gewichtet werden, sie funktioniert ausschliesslich nach dem 
 * firstIn-firstOut Prinzip
 * 
 * @author Oliver Kamrath
 *
 * @param <E> - zu speichernder Datentyp
 */
public class PriorityQueue<E extends Comparable<E>> implements QueueInt<E>{
	
	/**
	 * <p>
	 * Aktuelle Anzahl an Elementen in der Queue.
	 * </p>
	 */
	private int size;
	/**
	 * <p>
	 * Speicherkapazitaet der Queue.
	 * </p>
	 */
	private int speicherGroesse;
	/**
	 * <p>
	 * Datenspeicher der Queue.
	 * </p>
	 */
	private E[] speicher;

	/**
	 * <p>
	 * Zeiger auf das erste Element in der Queue.
	 * </p>
	 */
	private int zeigerAnfang; 
	/**
	 * <p>
	 * Zeiger auf den ersten freien Platz in der Queue.
	 * </p>
	 */
	private int zeigerFrei; 

	/**
	 * Konstruktor des Objekts. 
	 * Speichergroesse muss groesser 0 und kleiner 100 sein, ansonsten wird eine 
	 * IllegalArgumentException geworfen.
	 * @param speicherGroesse - Groesse des Speichers der Queue
	 */
	@SuppressWarnings("unchecked")
	public PriorityQueue(int speicherGroesse) {
		
		if(speicherGroesse <= 0 || speicherGroesse > 100) {
			throw new IllegalArgumentException();
		}
		
		speicher = (E[]) new Comparable[speicherGroesse];
		this.speicherGroesse = speicherGroesse;
		zeigerAnfang = 0;
		zeigerFrei = 0;
		size = 0;
	}

	public boolean einfuegen(E e) {
		
		if (this.size() == this.getSpeicherSize()) {
			return false;
		}
		
		speicher[zeigerFrei] = e;
		size++;
		zeigerFrei++;
		if (zeigerFrei >= speicherGroesse) {
			zeigerFrei = 0;
		}
		
		return true;
	}

	public E entfernen() {
		if (isEmpty()) {
			return null;
		}
		E e = speicher[zeigerAnfang];
		zeigerAnfang++;
		size--;
		if (zeigerAnfang >= speicherGroesse) {
			zeigerAnfang = 0;
		}
		return e;
	}

	public E erstes() {
		if (isEmpty()) {
			return null;
		}
		return speicher[zeigerAnfang];
	}

	public int size() {
		return size;
	}

	public int getSpeicherSize() {
		return speicherGroesse;
	}

	public boolean isEmpty() {
		if (size == 0) {
			return true;
		} else {
			return false;
		}
	}

	public E getElement(int index) {
		
		if (index >= speicherGroesse || index >= size) {
			return null;
		}
		
		int nindex = zeigerAnfang + index; 
		if(nindex >= speicherGroesse) {
			nindex = index - (speicherGroesse - (zeigerAnfang));
			return speicher[nindex];
		} else {
			return speicher[nindex];
		}
	}

	/**
	 * <p>
	 * Gibt den Wert des Zeigers zurueck der auf das erste Element in der Queue zeigt.
	 * </p>
	 * @return Wert von zeigerAnfang
	 */
	public int getZeigerAnfang() {
		return zeigerAnfang;
	}
	
	/**
	 * <p>
	 * Gibt den Wert des Zeigers zurueck der auf die erste freie Stelle der Queue zeigt.
	 * </p>
	 * 
	 * @return Wert von zeigerFrei
	 */
	public int getZeigerFrei() {
		return zeigerFrei;
	}
	
	/**
	 * 
	 * <p>
	 * Aendert den Speicher zu dem neuen �bergebenen Speicher,
	 * zusaetzlich werden mit zeigerAnfang und zeigerFrei die zeiger entsprechend angepasst.
	 * </p>
	 * <p>
	 * Falls der neue speicher nicht gleich gross wie der alte ist wird er nicht geaendert und es wird 1
	 * zurueckgeben.
	 * </p>
	 * <p>
	 * Es wird 2/3 zurueckgeben, falls der zeigerAnfang und zeigerFrei ausserhalb der Spezifikationen liegt.
	 * (zu gross oder zu klein)
	 * </p>
	 * <p>
	 * Es wird empfohlen ZeigerAnfang auf 0 zu setzten.
	 * </p>
	 * Wenn der Speicher erfolgreich ge�ndert wurde wird true zurueckgegeben.
	 * </p>
	 * 
	 * @param speicher - ein Array das die Element der Queue speichert
	 * @param zeigerAnfang - zeiger auf das erste Element im Array
	 * @param zeigerFrei - zeiger auf den ersten freien Platz im Array
	 * @return 0 - funtioniert, 1 - Speicher falsche groesse, 2 - ZeigerAnfang au�erhalb Spezifikation, 3 - ZeigerFrei au�erhalb Spezifikation
	 */
	protected int setSpeicher(E[] speicher, int zeigerAnfang, int zeigerFrei) {
		if(!(zeigerAnfang >= 0 && zeigerAnfang < speicherGroesse)) {
			return 2;
		}
		if(!(zeigerFrei >= 0 && zeigerFrei < speicherGroesse)) {
			return 3;
		}
		if(!(speicher.length == speicherGroesse)) {
			return 1;
		}
		
		this.speicher = speicher;
		this.zeigerAnfang = zeigerAnfang;
		this.zeigerFrei = zeigerFrei;
		return 0;
	}
	
	/**
	 * <p>
	 * Aendert die Size, also den Wert der Anzahl der Elemente in der Queue.
	 * </p>
	 * <p>
	 * Wenn dieser groe�er als die Speichergroesse ist wird false zurueckgegeben,
	 * sonst true.
	 * </p>
	 * 
	 * @param size - neue groesse der Queue
	 * @return true oder false
	 */
	protected boolean setSize(int size) {
		if( size >= speicherGroesse) {
			return false;
		}
		this.size = size;
		return true;
	}
	
	@Override
	public String toString() {
		String s = "";
		
		for(int i = 0; i < this.size; i++) {
			s += getElement(i);
			s += "|";
		}
		s += "\n Zeiger Anfang: " + this.zeigerAnfang;
		s += "\n Zeiger Frei: " + this.zeigerFrei;
		return s;
	}
}