package queue;

import java.util.NoSuchElementException;

/**
 * Testfaelle fuer die Queue und alle ihre Klassen
 * 
 * @author Oliver Kamrath
 *
 */
public class TestQueue {

	/**
	 * Erstellte Testfaelle und fuehrt diese aus.
	 * 
	 * @param args - leer lassen
	 */
	public static void main(String[] args) {

		// Test Priority Queue

		System.out.println("Test 1 Start Priority Queue");

		// Konstruktor Test
		try {
			PriorityQueue<Integer> pq = new PriorityQueue<Integer>(-1);
			System.out.println("FEHLER Konsturktor");
		} catch (IllegalArgumentException e) {
			
		}

		try {
			PriorityQueue<Integer> pq = new PriorityQueue<Integer>(101);
			System.out.println("FEHLER Konsturktor");
		} catch (IllegalArgumentException e) {

		}

		PriorityQueue<Integer> priorityQueue = new PriorityQueue<Integer>(5);

		assert priorityQueue.getSpeicherSize() == 5;

		// Empty + Size Test leer
		assert priorityQueue.isEmpty() == true;
		assert priorityQueue.size() == 0;

		// Erstes Test wenn leer
		assert priorityQueue.erstes() == null;

		// Einfuegen Test
		assert priorityQueue.einfuegen(1) == true;

		// Erstes Test
		assert priorityQueue.erstes() == 1;

		// Empty + Size Test mit Element
		assert priorityQueue.isEmpty() == false;
		assert priorityQueue.size() == 1;

		// Einfuegen Test bis voll
		assert priorityQueue.einfuegen(2) == true;
		assert priorityQueue.einfuegen(3) == true;
		assert priorityQueue.einfuegen(4) == true;
		assert priorityQueue.einfuegen(5) == true;

		// Einfuegen Test wenn voll
		assert priorityQueue.einfuegen(5) == false;

		// Empty + Size Test leer
		assert priorityQueue.isEmpty() == false;
		assert priorityQueue.size() == 5;

		// Test entfernen bis leer
		assert priorityQueue.entfernen() == 1;
		assert priorityQueue.entfernen() == 2;
		assert priorityQueue.entfernen() == 3;
		assert priorityQueue.entfernen() == 4;
		assert priorityQueue.entfernen() == 5;
		assert priorityQueue.isEmpty() == true;
		assert priorityQueue.entfernen() == null;

		// Test getElement

		assert priorityQueue.getElement(1) == null;

		priorityQueue.einfuegen(2);
		priorityQueue.einfuegen(3);

		assert priorityQueue.getElement(0) == 2;
		assert priorityQueue.getElement(1) == 3;

		System.out.println("End Test 1 Priority Queue");

		System.out.println("Starte Test 2 Priority Queue");

		// SpeicherGroesse und Anfangsvariablen
		PriorityQueue<Integer> pq = new PriorityQueue<Integer>(1);
		assert pq.getSpeicherSize() == 1;
		assert pq.getZeigerAnfang() == 0;
		assert pq.getZeigerFrei() == 0;
		assert pq.size() == 0;
		assert pq.isEmpty() == true;

		// Einfuegen, Ausgeben und Variablen;
		pq = new PriorityQueue<Integer>(10);

		// ein Element
		assert pq.einfuegen(10) == true;
		assert pq.size() == 1;
		assert pq.getZeigerAnfang() == 0;
		assert pq.getZeigerFrei() == 1;
		assert pq.isEmpty() == false;
		assert pq.getElement(0).compareTo(10) == 0;
		assert pq.erstes().compareTo(10) == 0;
		assert pq.entfernen().compareTo(10) == 0;

		// Meherer Elemente
		assert pq.einfuegen(1) == true;
		assert pq.getZeigerAnfang() == 1;
		assert pq.getZeigerFrei() == 2;
		assert pq.einfuegen(2) == true;
		assert pq.getZeigerAnfang() == 1;
		assert pq.getZeigerFrei() == 3;
		assert pq.einfuegen(3) == true;
		assert pq.getZeigerAnfang() == 1;
		assert pq.getZeigerFrei() == 4;
		assert pq.einfuegen(4) == true;
		assert pq.getZeigerAnfang() == 1;
		assert pq.getZeigerFrei() == 5;
		assert pq.einfuegen(5) == true;

		assert pq.size() == 5;
		assert pq.getZeigerAnfang() == 1;
		assert pq.getZeigerFrei() == 6;
		assert pq.isEmpty() == false;
		assert pq.getElement(0).compareTo(1) == 0;
		assert pq.getElement(1).compareTo(2) == 0;
		assert pq.getElement(2).compareTo(3) == 0;
		assert pq.getElement(3).compareTo(4) == 0;
		assert pq.getElement(4).compareTo(5) == 0;
		assert pq.getElement(5) == null;
		assert pq.erstes().compareTo(1) == 0;
		assert pq.entfernen().compareTo(1) == 0;

		// Volle Queue
		pq = new PriorityQueue<Integer>(5);

		// Einfuegen
		assert pq.einfuegen(1) == true;
		assert pq.einfuegen(2) == true;
		assert pq.einfuegen(3) == true;
		assert pq.einfuegen(4) == true;
		assert pq.einfuegen(5) == true;

		assert pq.getSpeicherSize() == 5;
		assert pq.isEmpty() == false;
		assert pq.size() == 5;
		assert pq.getZeigerAnfang() == 0;
		assert pq.getZeigerFrei() == 0;
		assert pq.erstes().compareTo(1) == 0;

		assert pq.getElement(0).compareTo(1) == 0;
		assert pq.getElement(1).compareTo(2) == 0;
		assert pq.getElement(2).compareTo(3) == 0;
		assert pq.getElement(3).compareTo(4) == 0;
		assert pq.getElement(4).compareTo(5) == 0;
		assert pq.getElement(5) == null;

		// Entfernt diese wieder

		assert pq.entfernen().compareTo(1) == 0;
		assert pq.getZeigerAnfang() == 1;
		assert pq.getZeigerFrei() == 0;
		assert pq.entfernen().compareTo(2) == 0;
		assert pq.getZeigerAnfang() == 2;
		assert pq.getZeigerFrei() == 0;
		assert pq.entfernen().compareTo(3) == 0;
		assert pq.getZeigerAnfang() == 3;
		assert pq.getZeigerFrei() == 0;
		assert pq.entfernen().compareTo(4) == 0;
		assert pq.getZeigerAnfang() == 4;
		assert pq.getZeigerFrei() == 0;
		assert pq.entfernen().compareTo(5) == 0;
		assert pq.getZeigerAnfang() == 0;
		assert pq.getZeigerFrei() == 0;

		assert pq.isEmpty() == true;

		assert pq.setSize(6) == false;
		pq.setSize(1);
		assert pq.size() == 1;

		// getElement
		pq = new PriorityQueue<Integer>(3);
		pq.einfuegen(2);
		assert pq.getElement(0) == 2;

		// fuer den Fall, dass der ZeigerAnfang + Index von vorne beginnt

		pq.einfuegen(3);
		pq.einfuegen(5);
		pq.entfernen();
		pq.einfuegen(8);
		assert pq.getElement(2) == 8;
		assert pq.getZeigerAnfang() == 1;

		// Neuer Speicher
		pq = new PriorityQueue<Integer>(3);
		Integer[] newSpeicher = { 1, 2, 3 };
		assert newSpeicher.length == pq.getSpeicherSize();
		assert pq.setSpeicher(newSpeicher, 0, 0) == 0;

		System.out.println("Priority Queue erfolgreich");

		// Utility Queue

		QueueUtility<Integer> uq = new QueueUtility<Integer>(5);

		assert uq.getSpeicherSize() == 5;
		assert uq.isEmpty() == true;
		assert uq.size() == 0;
		assert uq.getZeigerAnfang() == 0;
		assert uq.getZeigerFrei() == 0;

		assert uq.einfuegen(1) == true;
		assert uq.getZeigerAnfang() == 0;
		assert uq.getZeigerFrei() == 1;
		assert uq.einfuegen(2) == true;
		assert uq.getZeigerFrei() == 2;
		assert uq.einfuegen(3) == true;
		assert uq.getZeigerFrei() == 3;
		assert uq.einfuegen(4) == true;
		assert uq.getZeigerFrei() == 4;
		assert uq.einfuegen(5) == true;
		assert uq.einfuegen(6) == false;

		assert uq.isEmpty() == false;
		assert uq.size() == 5;
		assert uq.getZeigerAnfang() == 0;
		assert uq.getZeigerFrei() == 0;

		// Search

		try {
			uq.search(10);
			System.out.println("Search funktioniert nicht");
		} catch (NoSuchElementException e) {
		}
		
		int[] searchArrayResult;
		searchArrayResult = uq.search(1);
		assert searchArrayResult[0] == 0;
		
		searchArrayResult = uq.search(2);
		assert searchArrayResult[0] == 1;
		searchArrayResult = uq.search(3);
		assert searchArrayResult[0] == 2;
		searchArrayResult = uq.search(4);
		assert searchArrayResult[0] == 3;
		searchArrayResult = uq.search(5);
		assert searchArrayResult[0] == 4;
		
		uq.entfernen();
		uq.einfuegen(2);
		searchArrayResult = uq.search(2);
		assert searchArrayResult[0] == 0;
		assert searchArrayResult[1] == 4;


		// update
		assert uq.update(10, 10) == 5;
		assert uq.update(6, 0) == 0;
		assert uq.update(7, 1) == 0;
		assert uq.update(8, 2) == 0;
		assert uq.update(9, 3) == 0;
		assert uq.update(10, 4) == 0;

		assert uq.entfernen() == 6;
		assert uq.entfernen() == 7;
		assert uq.entfernen() == 8;
		assert uq.entfernen() == 9;
		assert uq.entfernen() == 10;

		assert uq.isEmpty();

		// insert

		uq = new QueueUtility<Integer>(5);
		assert uq.insert(10, 2) == 7;

		assert uq.getZeigerAnfang() == 0;
		assert uq.getZeigerFrei() == 0;
		assert uq.einfuegen(1) == true;
		assert uq.getZeigerAnfang() == 0;
		assert uq.getZeigerFrei() == 1;
		assert uq.einfuegen(2) == true;
		assert uq.getZeigerAnfang() == 0;
		assert uq.getZeigerFrei() == 2;
		assert uq.einfuegen(4) == true;
		assert uq.getZeigerAnfang() == 0;
		assert uq.getZeigerFrei() == 3;
		assert uq.size() == 3;

		assert uq.insert(3, 2) == 0;
		assert uq.size() == 4;
		assert uq.getZeigerAnfang() == 0;
		assert uq.getZeigerFrei() == 4;

		assert uq.entfernen() == 1;
		assert uq.size() == 3;
		assert uq.getZeigerAnfang() == 1;

		assert uq.entfernen() == 2;
		assert uq.size() == 2;
		assert uq.getZeigerAnfang() == 2;

		assert uq.entfernen() == 3;
		assert uq.size() == 1;
		assert uq.getZeigerAnfang() == 3;

		assert uq.entfernen() == 4;
		assert uq.size() == 0;
		assert uq.getZeigerAnfang() == 4;

		// delete

		uq = new QueueUtility<Integer>(5);
		assert uq.delete(2) == 8;

		assert uq.getZeigerAnfang() == 0;
		assert uq.getZeigerFrei() == 0;
		assert uq.einfuegen(1) == true;
		assert uq.getZeigerAnfang() == 0;
		assert uq.getZeigerFrei() == 1;
		assert uq.einfuegen(2) == true;
		assert uq.getZeigerAnfang() == 0;
		assert uq.getZeigerFrei() == 2;
		assert uq.einfuegen(3) == true;
		assert uq.getZeigerAnfang() == 0;
		assert uq.getZeigerFrei() == 3;
		assert uq.size() == 3;

		assert uq.delete(1) == 0;
		assert uq.size() == 2;

		assert uq.entfernen() == 1;
		assert uq.entfernen() == 3;
		
		// test toArray
		uq = new QueueUtility<Integer>(5);
		uq.einfuegen(0);
		uq.einfuegen(1);
		uq.einfuegen(2);
		
		Integer[] testArray = {0,1,2};
		Comparable<Integer>[] queueArray = uq.toArray();
		
		for(int i = 0; i < testArray.length; i++) {
			assert testArray[i] == queueArray[i];
		}
		

		System.out.println("Die Normalen Methoden der Utlity Queue wurden getestet");

		// Sortier Algorithmus

		QueueSorter<Integer> sq = new QueueSorter<Integer>(5);
		assert sq.einfuegen(1) == true;
		assert sq.einfuegen(2) == true;
		assert sq.einfuegen(3) == true;
		assert sq.einfuegen(4) == true;
		assert sq.einfuegen(5) == true;
		assert sq.shakerSort() == 0;
		assert sq.entfernen().equals(1);
		assert sq.entfernen().equals(2);
		assert sq.entfernen().equals(3);
		assert sq.entfernen().equals(4);
		assert sq.entfernen().equals(5);

		assert sq.einfuegen(2) == true;
		assert sq.einfuegen(4) == true;
		assert sq.einfuegen(5) == true;
		assert sq.einfuegen(3) == true;
		assert sq.einfuegen(1) == true;
		assert sq.shakerSort() == 0;
		assert sq.entfernen().equals(1);
		assert sq.entfernen().equals(2);
		assert sq.entfernen().equals(3);
		assert sq.entfernen().equals(4);
		assert sq.entfernen().equals(5);

		assert sq.einfuegen(20) == true;
		assert sq.einfuegen(14) == true;
		assert sq.einfuegen(44) == true;
		assert sq.shakerSort() == 0;
		assert sq.entfernen().equals(14);
		assert sq.entfernen().equals(20);
		assert sq.entfernen().equals(44);

		assert sq.size() == 0;

		assert sq.einfuegen(20) == true;
		assert sq.einfuegen(14) == true;
		assert sq.einfuegen(14) == true;
		assert sq.getElement(0) == 20;
		assert sq.getElement(1) == 14;
		assert sq.getElement(2) == 14;
		assert sq.shakerSort() == 0;
		assert sq.entfernen().equals(14);
		assert sq.entfernen().equals(14);
		assert sq.entfernen().equals(20);

		assert sq.size() == 0;
		assert sq.einfuegen(5) == true;
		assert sq.shakerSort() == 0;
		assert sq.entfernen().equals(5);

		System.out.println("Der sortierAlgorithmus geht");

	}

}
