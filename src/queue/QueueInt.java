package queue;

/**
 * Interface fuer eine Queue, beinhaltet alle standard Operationen.
 * @author Oliver Kamrath
 *
 * @param <E> zu speichernder Datentyp
 */
public interface QueueInt<E extends Comparable<E>> {
	
	/**
	 * <p>
	 * Fuegt ein Element an das Ende der Warteschlange an. 
	 * </p>
	 * <p>
	 * Falls die Queue voll ist, wird abgebrochen und false zurueckgegeben. 
	 * </p>
	 * <p>
	 * Wenn das Element angefuegt werden konnte wird true zurueckgegeben.
	 * </p>
	 * 
	 * @param e Wert zum einfuegen 
	 * @return false oder true 
	 */
	boolean einfuegen(E e); 
	
	/**
	 * <p>
	 * Entfernt das erste Element aus der Queue.
	 * </p>
	 * <p>
	 * Falls die Queue leer ist, wird null zur�ckgegeben.
	 * </p>
	 * <p>
	 * Andernfalls das erste Element.
	 * </p>
	 * 
	 * @return null oder das erste Element
	 */
	E entfernen(); 
	
	/**
	 * <p>
	 * Gibt das erste Element aus der Queue aus.
	 * </p>
	 * <p>
	 * Falls die Queue leer ist wird null zurueckgegeben.
	 * </p>
	 * <p>
	 * Andernfalls das Erste.
	 * </p>
	 * 
	 * @return null oder das erste Element
	 */
	E erstes(); 
	
	/**
	 * <p>
	 * Gibt die Groesse des Speichers der Queue zurueck.
	 * </p>
	 * 
	 * @return die Speichergroe�e des arrays
	 */
	int getSpeicherSize();
	
	/**
	 * <p>
	 * Gibt die Anzahl der Elemente in der Queue zurueck.
	 * </p>
	 * 
	 * @return anzahl an Elementen in der Queue
	 */
	int size(); 
	
	/**
	 * <p>
	 *  Gibt true zurueck, wenn die Queue leer ist und sonst false.
	 * </p>
	 * 
	 * @return true or false
	 */
	boolean isEmpty(); 
	
	/**
	 * <p>
	 * Gibt das Element an der Stelle des Indexes zurueck, ausgehend vom ersten Element.
	 * </p>
	 * 
	 * @param index - ausgehend von dem ersten Element
	 * @return null oder das Element
	 */
	E getElement(int index);
}
