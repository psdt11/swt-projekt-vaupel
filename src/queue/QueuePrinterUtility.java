package queue;

/**
 * A class containing predefinde ways to print a queue
 * @author cfz4x
 *
 * @param <E>
 */
public class QueuePrinterUtility<E extends Comparable<E>> extends QueuePrinter<E> {

	public QueuePrinterUtility(QueueSorter<E> qu) {
		super(qu);
	}

	public int printBasicQueue(String heading, String subheading) {
		int errorCode = 0;
		this.setSubHeading(subheading);
		this.setHeading(heading);
		this.setExplanation(true);
		errorCode = this.printQueueToSameFile();
		if(errorCode != 0) {
			return errorCode;
		}
		clear();
		return errorCode;
	}
	
 	public int printAppend() {
		int errorCode = 0;
		if(!this.setHeading("Einfuegen an das Ende der Queue (Append) ")) {
			return 17;
		}
		if(!this.setIndexeFarbe(new int[] {this.qu.size() -1}, new String [] {"red"})) {
			return 16;
		}
		errorCode = this.setExtendedExplanation(new String[] {"append", "rot", "appended"});
		if(errorCode != 0) {
			return errorCode;
		}
		this.setExplanation(true);
		
		
		errorCode = this.printQueueToSameFile();
		if(errorCode != 0) {
			return errorCode;
		}
		clear();
		return errorCode;
	}
	
 	public int printInsert(int indexInsert) {
 		int errorCode = 0;
		if(!this.setHeading("Queue nach dem Insert")) {
			return 17;
		}
		if(!this.setIndexeFarbe(new int[] {indexInsert}, new String [] {"red"})) {
			return 16;
		}
		errorCode = this.setExtendedExplanation(new String[] {"insert", "rot", "inserted"});
		if(errorCode != 0) {
			return errorCode;
		}
		this.setExplanation(true);
		
		
		errorCode = this.printQueueToSameFile();
		if(errorCode != 0) {
			return errorCode;
		}
		clear();
		return errorCode;
 	}
	
 	public int printRemoveFirst() {
 		int errorCode = 0;
		if(!this.setHeading("Queue nach dem Enfernen des ersten Elements")) {
			return 17;
		}
		this.setExplanation(true);
		
		
		errorCode = this.printQueueToSameFile();
		if(errorCode != 0) {
			return errorCode;
		}
		clear();
		return errorCode;
 	}
 	
 	public int printBeforeDelete(int index) {
 		int errorCode = 0;
 		
 		if(!this.setHeading("Queue vor dem Loeschen an Indes ")) {
			return 17;
		}
		if(!this.setIndexeFarbe(new int[] {index}, new String [] {"red"})) {
			return 16;
		}
		errorCode = this.setExtendedExplanation(new String[] {"delete", "rot", "to delete"});
		if(errorCode != 0) {
			return errorCode;
		}
		this.setExplanation(true);
		
		errorCode = this.printQueueToSameFile();
		if(errorCode != 0) {
			return errorCode;
		}
		clear();
		return errorCode;
 	}
 	
 	public int printAfterDelete(int index) {
 		int errorCode = 0;
 		
 		if(!this.setHeading("Queue nach dem Loeschen an Index ")) {
			return 17;
		}
		if(!this.setIndexeFarbe(new int[] {index, index-1}, new String [] {"blue", "blue"})) {
			return 16;
		}
		errorCode = this.setExtendedExplanation(new String[] {"deleted", "blue", "between"});
		if(errorCode != 0) {
			return errorCode;
		}
		this.setExplanation(true);
		
		errorCode = this.printQueueToSameFile();
		if(errorCode != 0) {
			return errorCode;
		}
		clear();
		return errorCode;
 	}
 	
 	public int printUpdate(boolean before, int index) {
 		int errorCode = 0;
 		String heading;
 		String des;
 		
 		if(before) {
 			heading = "Queue vor dem Update an Index";
 			des = "to update";
 		} else {
 			heading = "Queue nach dem Update an Index";
 			des = "updated";
 		}
 		
 		
		if(!this.setHeading(heading)) {
			return 17;
		}
		if(!this.setIndexeFarbe(new int[] {index}, new String [] {"red"})) {
			return 16;
		}
		errorCode = this.setExtendedExplanation(new String[] {"update", "rot", des});
		if(errorCode != 0) {
			return errorCode;
		}
		this.setExplanation(true);
		
		
		errorCode = this.printQueueToSameFile();
		if(errorCode != 0) {
			return errorCode;
		}
		clear();
		return errorCode;
 	}
 	
 	public int printSearch(int[] indexe) {
 		int errorCode = 0;
 		
 		String[] farben = new String[indexe.length];
 		
 		for(int i = 0; i < farben.length; i++) {
 			farben[i] = "blue";
 		}
 		
 		
		if(!this.setHeading("Such ergebnis in the Queue")) {
			return 17;
		}
		
		if(!this.setIndexeFarbe(indexe, farben)) {
			return 16;
		}
		errorCode = this.setExtendedExplanation(new String[] {"search", "blau", "result"});
		if(errorCode != 0) {
			return errorCode;
		}
		this.setExplanation(true);
		
		
		errorCode = this.printQueueToSameFile();
		if(errorCode != 0) {
			return errorCode;
		}
		clear();
		return errorCode;
 		
 	}
 	
 	public void clear() {
		setHeading("");
		setSubHeading("");
		setExplanation(false);
		setExtendedExplanation(null);
		setIndexeFarbe(null, null);
	}
	

}
