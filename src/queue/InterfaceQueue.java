package queue;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * 
 * 
 * 
 * 
 * 
 * ERROR WERTE:
 * <p>
 * 0 - funktioniert
 * </p>
 * <p>
 * 1 - Speicher falsche groesse (critical)
 * </p>
 * <p>
 * 2 - ZeigerAnfang falsch (critical)
 * </p>
 * <p>
 * 3 - ZeigerFrei falsch (critical)
 * </p>
 * <p>
 * 4 - Speicher voll
 * </p>
 * <p>
 * 5 - Index groesser als der Speicher
 * </p>
 * <p>
 * 6 - Index kleiner 0
 * </p>
 * <p>
 * 7 - Index ausserhalb der Queue +1
 * </p>
 * <p>
 * 8 - Queue leer
 * </p>
 * <p>
 * 9 - File bereits erstellt, aber nicht beendet
 * </p>
 * <p>
 * 10 - IOException bei Filwriter bzw. buffereWriter
 * </p>
 * <p>
 * 11 - File noch nicht erstellt
 * </p>
 * <p>
 * 12 - text empty
 * </p>
 * <p>
 * 13 - text too big
 * </p>
 * <p>
 * 14 - explanation too long
 * </p>
 * <p>
 * 15 - extendedExplanation ungueltig
 * </p>
 * <p>
 * 16 - setIndexFarben error
 * </p>
 * <p>
 * 17 - Ueberschrift error
 * </p>
 * <p>
 * 18 - ort Variable ungueltig
 * </p>
 * <p>
 * 20 - nicht gesetzt Array ungueltige Groe�e
 * </p>
 * <p>
 * 21 - ein/ mehere Parts falsch eingegeben, trotzdem gesetzt
 * </p>
 * <p>
 * 22 - falsche Print Methode aufgerufen
 * </p>
 * <p>
 * 23 - null/ parts auf false gesetzt
 * </p>
 * <p>
 * 24 - Shake Sort noch nicht ausgefuehrt
 * </p>
 */
public class InterfaceQueue {

	// Varibalen zum Printen
	QueueSorter<Integer> qs;
	QueuePrinterUtility<Integer> qp;

	private String filename = "";
	boolean printToPDF = false;
	boolean pdfStarted = false;

	// Variablen fuer Input
	int input;
	Scanner sc = new Scanner(System.in);

	int choice = 0;

	private int[] parts = null;

	int maxSize = 0;

	boolean running = true;

	public InterfaceQueue() {
		setMaxSize();
		qp = new QueuePrinterUtility<Integer>(qs);
	}

	public void start() {

		while (running) {

			setChoice();

			setToPrint();

			menu();

		}
	}

	public void setChoice() {

		boolean validInput = false;

		System.out.println("What do you want to do?: ");
		System.out.println("1  For printing out the Queue to Console \n"
				+ "2  For appending an Elemnt at the End of the Queue \n"
				+ "3  For appending n Random Values to the End of the Queue \n"
				+ "4  For inserting Element at the desired Place \n"
				+ "5  For removing the first Element from the Queue \n"
				+ "6  For deleting an Element at the desired Place \n" + "7  For deleting all Elments from the Queue \n"
				+ "8  For changing an Element at the desired Place \n"
				+ "8  For viewing the First Element of the Queue \n" + "9  For viewing Elment at the desired Place \n"
				+ "10 For searching for an Element in the Queue \n"
				+ "11 To sort the Queue based on the Shaker Sort Algorithm\n"
				+ "12 To view the current size of the Queue \n" + "13 To view the current memeory size of the Queue \n"
				+ "14 To change the Max Size and creating a new Queue \n"
				+ "15 To close the Current File (if one exists) \n" + "16 To exist the Queue Interface \n");

		while (!validInput) {
			while (!getNumberInput()) {
				System.out.println("Input illegal, please enter one of the numbers above (1-16)");
			}

			if (input < 1 || input > 16) {
				System.out.println("Input illegal, please enter one of the numbers above (1-16)");
				validInput = false;
			} else {
				validInput = true;
			}
		}
		choice = input;

		return;
	}

	private void setToPrint() {
		

		boolean validInput = false;
		printToPDF = false;

		if (choice == 1 || choice == 3 || choice == 7 || choice == 9 || choice == 12 || choice == 13 || choice == 14
				|| choice == 15) {
			printToPDF = false;
		} else {
			System.out.println("Do you want to Print the result as PDF? yes = 1, no = 0");
			while (!validInput) {
				while (!getNumberInput()) {
					System.out.println("Input illegal, please enter one of the numbers above (0/1)");
				}

				if (input < 0 || input > 1) {
					System.out.println("Input illegal, please enter one of the numbers above (0/1)");
					validInput = false;
				} else {
					validInput = true;
				}
			}

			if (input == 1) {
				printToPDF = true;
			} else {
				printToPDF = false;
			}
		}

		if (printToPDF && choice == 12) {
			qp.endFile();
			System.out.println("The Shaker Sort Algorithm needs to be printed in its own file");
			startFile();
		} else if (printToPDF) {
			continueFile();
			setParts();
		}

	}

	private void menu() {

		switch (choice) {
		case 1:
			System.out.println(qs.toString());
			break;
		case 2:
			append();
			break;
		case 3:
			appendRandomNumber();
			break;
		case 4:
			insert();
			break;
		case 5:
			remove();
			break;
		case 6:
			delete();
			break;
		case 7:
			deleteAll();
			break;
		case 8:
			update();
			break;
		case 9:
			if (qs.isEmpty()) {
				System.out.println("The Queue is empty ");
			} else {
				System.out.println("The first Element of the Queue is  " + qs.erstes());
			}
			break;
		case 10:
			search();
			break;
		case 11:
			sort();
			break;
		case 12:
			System.out.println("The current size of the Queue is " + qs.size());
			break;
		case 13:
			System.out.println("The current memoery size of the Queue is " + qs.getSpeicherSize());
			break;
		case 14:
			setMaxSize();
			break;
		case 15:
			System.out.println("Ending the current file if there is one existing ");
			qp.endFile();
			break;
		case 16:
			System.out.println("Exiting the queue interface");
			qp.endFile();
			running = false;
			break;
		}

	}

	private void append() {
		System.out.println("Append:");
		int errorCode = 0;
		readElementInput("Pls enter a value to append to the Queue: ");
		if (!qs.einfuegen(input)) {
			System.out.println("Queue is full, cant append element");
			return;
		} else {
			System.out.println("Put element " + input + " at the end of the Queue");
		}

		if (printToPDF) {
			setParts();
			errorCode = qp.printAppend();
			errorHandler(errorCode);
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}
			System.out.println("Printed the queue to pdf");
		}
	}

	private void appendRandomNumber() {
		System.out.println("Appand random numbers: ");
		boolean validInput = false;
		int number;
		System.out.println("How many numbers do you want to add: ");

		while (!validInput) {
			while (!getNumberInput()) {
				System.out.println("Input illegal, please enter a number ");
			}

			if (input < 0 || input > (qs.getSpeicherSize() - qs.size())) {
				System.out.println("Input illegal, number too small or too big, must be between 0 and "
						+ (qs.getSpeicherSize() - qs.size()) + " since the queue would be full otherwise");
				validInput = false;
			} else {
				validInput = true;
			}
		}

		for (int i = 0; i < input; i++) {
			number = (int) (Math.random() * 100);
			qs.einfuegen(number);
		}

	}

	private void insert() {
		System.out.println("Insert element at Index: ");
		int errorCode = 0;
		int inputElement;
		
		if (qs.isEmpty()){
			System.out.println("The queue is empty, cant insert anything");
			return;
		}

		readElementInput("Pls enter a value to insert into the Queue: ");
		inputElement = input;

		readIndexInput();

		if (printToPDF) {

			errorCode = qp.printBasicQueue("Queue vor dem Insert", "");
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}
			errorCode = qs.insert(inputElement, input);
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}
			errorCode = qp.printInsert(input);
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}

			System.out.println("Printed the queue to pdf");

		} else {
			errorCode = qs.insert(inputElement, input);
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}
		}

		System.out.println("Put element " + inputElement + " at " + input + " index  of the Queue");

	}

	private void remove() {

		System.out.println("Removing the first element of the queue: ");
		int errorCode = 0;
		int output = 0;

		if (qs.isEmpty()) {
			System.out.println("The queue is empty, there is nothing to remove");
			return;
		}

		if (printToPDF) {

			errorCode = qp.printBasicQueue("Queue bevor das erste Element entfernt wurde", "");
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}
			output = qs.entfernen();
			errorCode = qp.printRemoveFirst();
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}

			System.out.println("Queue printed to pdf");
		} else {
			output = qs.entfernen();
		}

		System.out.println("The first Element " + output + " has been removed");

	}

	private void delete() {
		System.out.println("Deleting an element at Index from queue");
		int errorCode = 0;

		if (qs.isEmpty()) {
			System.out.println("Queue is empty, there is nothing to delete");
			return;
		}

		readIndexInput();

		if (printToPDF) {

			errorCode = qp.printBeforeDelete(input);
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}
			errorCode = qs.delete(input);
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}
			errorCode = qp.printAfterDelete(input);
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}

			System.out.println("Queue printed to pdf");

		} else {

			errorCode = qs.delete(input);
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}
		}

		System.out.println("The element at " + input + " has been deleted");
	}

	private void deleteAll() {

		System.out.println("Deleting all entries from the queue");
		qs = new QueueSorter<Integer>(qs.getSpeicherSize());
		System.out.println("All entries have been deleted");

	}

	private void update() {

		System.out.println("Updating an element at Index from queue");
		int errorCode = 0;

		int inputElement;

		if (qs.isEmpty()) {
			System.out.println("Queue is empty, there is nothing to update");
			return;
		}

		readElementInput("Pls enter a value to update the Queue with: ");
		inputElement = input;
		readIndexInput();

		if (printToPDF) {

			errorCode = qp.printUpdate(true, input);
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}
			errorCode = qs.update(inputElement, input);
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}
			errorCode = qp.printUpdate(false, input);
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}

			System.out.println("Printed queue to pdf");

		} else {

			errorCode = qs.update(inputElement, input);
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}
		}

		System.out.println("Updated element " + inputElement + " at " + input + " index  of the Queue");

	}

	private void search() {

		System.out.println("Searching an Element in the Queue");
		int errorCode = 0;

		if (qs.isEmpty()) {
			System.out.println("Queue is empty, there is nothing to search");
			return;
		}

		readElementInput("Pls enter a value to search in the queue: ");

		int[] result;

		try {
			result = qs.search(input);
		} catch (NoSuchElementException e) {
			System.out.println("There is no such Element in the queue");
			return;
		}

		if (printToPDF) {

			errorCode = qp.printSearch(result);
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}

			System.out.println("Printed queue to pdf");

		}

		System.out.println("The element " + input + " appears at the following positions: ");
		System.out.print("[");
		for (int i = 0; i < result.length; i++) {
			System.out.print(result[i]);
			System.out.print("  ");
		}
		System.out.print("]");

	}

	private void sort() {
		System.out.println("Sorting the queue based on the Shaker Sort Algortihm");
		int errorCode = 0;

		if (qs.isEmpty()) {
			System.out.println("The queue is empty, no need to sort it");
		}

		if (qs.size() == 1) {
			System.out.println("The queue contains only 1 element, no need to sort it");
		}

		if (qs.size() > 10) {
			System.out.println("Queue size is too big (> 10), cant be printed");
			printToPDF = false;

		}

		if (printToPDF) {

			errorCode = qs.shakerSort();
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}
			errorCode = qp.printQueueAlgorithm(filename);
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}
			System.out.println("Queue Shaker Sort has been printed to pdf");

		} else {
			errorCode = qs.shakerSort();
			if (errorCode != 0) {
				errorHandler(errorCode);
				return;
			}
		}

		System.out.println("The Queue has been sorted");
	}
	

	private void setParts() {
		
		qp.setParts(null);
		int errorCode = 0;

		/*
		 * Parts 0 && parts 1
		 * 
		 */
		int input = 0;
		boolean validInput = false;
		parts = null;

		while (!validInput) {
			System.out.println("Do you want to view the full queue or only parts of it? 1 = full queue / 0 = parts");
			input = sc.nextInt();
			if (input == 1 || input == 0) {
				validInput = true;
			}
		}

		validInput = false;

		if (input == 0) {
			while (!validInput) {
				System.out.println(
						"What do you want to set? (0 - startIndex, 1 - endIndex, 2 start-endIndex, 3-gap, 4 gap with start+end index");
				input = sc.nextInt();
				if (input > -1 && input < 5) {
					validInput = true;
				}
			}

			switch (input) {
			case 0:
				parts = new int[4];
				parts[0] = askValueParts("the queue start?");
				parts[1] = qs.size();
				break;
			case 1:
				parts = new int[4];
				parts[1] = askValueParts("the queue end?");
				break;
			case 2:
				parts = new int[4];
				parts[0] = askValueParts("the queue start?");
				parts[1] = askValueParts("the queue end?");
				break;
			case 3:
				parts = new int[4];
				parts[1] = qs.size();
				parts[2] = askValueParts("start a gap in the queue?");
				parts[3] = askValueParts("end a gap in the queue?");
				break;
			case 4:
				parts = new int[4];
				parts[0] = askValueParts("the queue start?");
				parts[1] = askValueParts("the queue end?");
				parts[2] = askValueParts("start a gap in the queue?");
				parts[3] = askValueParts("end a gap in the queue?");
				break;
			}

		} else {
			return;
		}
		
		qp.setParts(parts);
		if(errorCode != 0) {
			errorHandler(errorCode);
		}

	}

	private int askValueParts(String text) {

		boolean validInput = false;
		int input = 0;

		while (!validInput) {
			System.out.println("At what index should " + text);
			input = sc.nextInt();
			if (input > 0 && input < qs.size()) {
				validInput = true;
			} else {
				System.out.println("index too big / small");
			}
		}

		return input;
	}

	private void readIndexInput() {
		boolean validInput = false;

		System.out.println("Pls enter a index from the Queue: " + "(0 - " + (qs.size() - 1) + ")");

		while (!validInput) {
			while (!getNumberInput()) {
				System.out.println("Input illegal, please enter a number ");
			}

			if (input < 0 || input > (qs.size() - 1)) {
				System.out.println("Input illegal, a lower/bigger number needed (0 - " + (qs.size() - 1) + ")");
				validInput = false;
			} else {
				validInput = true;
			}
		}
	}

	private void readElementInput(String task) {
		boolean validInput = false;

		System.out.println(task);

		while (!validInput) {
			while (!getNumberInput()) {
				System.out.println("Input illegal, please enter a number ");
			}

			if (input < -999999 || input > 999999) {
				System.out.println("Input illegal, a lower/bigger number (+/- 999999 allowed) ");
				validInput = false;
			} else {
				validInput = true;
			}
		}
	}

	private void continueFile() {
		boolean validInput = false;
		boolean sameFILE = false;

		if (pdfStarted) {
			System.out.println("new file or continue file? (continue = 1/ new file = 0) ");

			while (!validInput) {
				while (!getNumberInput()) {
					System.out.println("Input illegal, please enter one of the numbers above (0/1)");
				}

				if (input < 0 || input > 1) {
					System.out.println("Input illegal, please enter one of the numbers above (0/1)");
					validInput = false;
				} else {
					validInput = true;
				}
			}

			if (input == 1) {
				sameFILE = true;
			} else {
				sameFILE = false;
			}

			if (sameFILE) {
			} else {
				qp.endFile();
				pdfStarted = false;
				startFile();
			}
		} else {
			pdfStarted = true;
			System.out.println("There is no File ");
			startFile();
		}

	}

	private void startFile() {
		int errorCode;
		String inputString = "";
		boolean validInput = false;

		while (!validInput) {
			System.out.println("Give the file a Name: ");
			inputString = sc.next();
			if (inputString.isEmpty() || inputString.length() > 50) {
				System.out.println("name too small  / too big");
			} else {
				validInput = true;
				filename = inputString;
			}
		}

		errorCode = qp.startFile(filename, 0);
		pdfStarted = true;

		errorHandler(errorCode);

	}

	private void setMaxSize() {
		boolean validInput = false;

		System.out.println("Enter the max memory size for the queue (1 - 100): ");

		while (!validInput) {
			while (!getNumberInput()) {
				System.out.println("Input illegal, must be a number");
			}

			try {
				qs = new QueueSorter<Integer>(input);
				validInput = true;
			} catch (IllegalArgumentException e) {
				System.out.println("Input illegal, number must be between 1 and 100");
			}
		}
	}

	private boolean getNumberInput() {
		try {
			input = sc.nextInt();
		} catch (InputMismatchException e) {
			sc.next();
			return false;
		}
		return true;
	}

	private void errorHandler(int errorCode) {
		String s = "";
		switch (errorCode) {
		case 0:
			return;
		case 1:
			s = "Memory wrong size (critical)";
			break;
		case 2:
			s = "PointerStart wrong (critical)";
			break;
		case 3: 
			s = "PointerFree wrong (critical)";
			break;
		case 4:
			s = "Memory full";
			break;
		case 5:
			s = "Index bigger than the memory";
			break;
		case 6:
			s = "Index smaller 0";
			break;
		case 7:
			s = " Index outdside of the Queue +1";
			break;
		case 8:
			s = "Queue empty";
			break;
		case 9:
			s = "File created, but not ended";
			break;
		case 10:
			s = " IOException in Filwriter or BufferedWriter";
			break;
		case 11:
			s = "File not created";
			break;
		case 12:
			s = "Text empty";
			break;
		case 13:
			s = "Text too big";
			break;
		case 14:
			s = "Explanation too long";
			break;
		case 15:
			s = "ExtendedExplanation invalid";
			break;
		case 16:
			s = "setIndexFarben threw Error";
			break;
		case 17: 
			s = "Heading threw error";
			break;
		case 18:
			s = "Place variable wrong";
			break;
		case 20:
			s = "index array wrong size, not activated";
			break;
		case 21:
			s = "one/ multiple parts of Parts are wrong, still activated parts";
			break;
		case 22:
			s = "wrong print method called";
			break;
		case 23:
			s = "null/ parts set to false";
			break;
		case 24:
			s = "Shake sort hasnt been called yet";
			break;
		}
		
		System.out.println("This error occurred: " + s);
		System.out.println("Going to menu");
	}
}
