package queue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


/**
 * Eine Klasse welche moelgichkeiten bietet, in eine Datei Tikz Code zu schreiben.
 * Es werden vorgefertigte Konstrukte wie Pfeil, Kaesten und Legende angeboten. 
 * Primaer darauf ausgelegt eine Queue darzustellen.
 * 
 * @author cfz4x
 *
 * @param <E> Datentyp welcher geprinted werden soll
 */
public class TikzWriter<E extends Comparable<E>> implements TikzWriterInt<E> {
	
	private File file;
	private FileWriter fileWriter;
	private BufferedWriter bw;
	private int countBlocks;
	private int countBlocksTotal;
	private double coordinateStartX;
	private double coordinateX;
	private double coordinateY;
	private double coordinateStartY;
	private StringBuilder sb;
	private int indexFiles = 0;
	private String filename;
	private boolean writeable = false;
	

	/**
	 * Erstellt einen neuen TikzWriter. Nach dem erstellen muss als erste Methode
	 * startFile() aufgerufen werden und am Ende die Methode endFile().
	 * 
	 */
	public TikzWriter() {
	}
	
	public int startFile(String filename, int indexFiles) {
		if (writeable) {
			return 9;
		}
		
		this.filename = filename;
		
		if(indexFiles != 0) {
			filename += indexFiles;
		}
		this.indexFiles = indexFiles;

		try {
			file = new File(filename + ".tikz");
			fileWriter = new FileWriter(file);
			bw = new BufferedWriter(fileWriter);
			bw.append("\\begin {tikzpicture} \n");
			bw.flush();
		} catch (IOException e) {
			return 10;
		}

		countBlocks = 0;
		coordinateStartX = 10;
		coordinateStartY = 10;
		coordinateX = 10;
		coordinateY = 10;
		sb = new StringBuilder();
		writeable = true;
		return 0;
	}
	 
	public int drawArrow(int block, String text) {

		if (!writeable) {
			return 11;
		}

		try {
			// calculate Position
			int numberRows = block / 10;
			int numberColumns = block % 10;

			double rectangleCoordinateY = coordinateStartY + (numberRows * -4);
			double rectangleCoordinateX = coordinateStartX + numberColumns;

			// Pfeil am Block
			sb.append("\\draw[->] (");
			sb.append(rectangleCoordinateX - 0.5);
			sb.append(",");
			sb.append(rectangleCoordinateY + 1);
			sb.append(") -- (");
			sb.append(rectangleCoordinateX - 0.5);
			sb.append(",");
			sb.append(rectangleCoordinateY);
			sb.append(") node[pos = -0.2] {");
			sb.append(text);
			sb.append("}; \n");

			bw.append(sb.toString());
			sb.setLength(0);
			bw.flush();
		} catch (IOException ioe) {
			return 10;
		}
		return 0;
	}

	public int drawDots() {
		return drawNewBlock(null, -1, "");
	}

	public int drawNewBlock(E wert, int index, String farbe) {

		if (!writeable) {
			return 11;
		}

		if (index == -1) {

			// Oberer Block
			if (!farbe.equals("")) {
				sb.append("\\fill[");
				sb.append(farbe);
				sb.append("!50!white"); // <----
				sb.append("] (");
			} else {
				sb.append("\\draw (");
			}

			sb.append(coordinateX);
			sb.append(",");
			sb.append(coordinateY);
			sb.append(") rectangle (");
			sb.append(coordinateX - 1);
			sb.append(",");
			sb.append(coordinateY - 1);
			sb.append("); \n");

			// Text im oberen Block
			sb.append("\\node at (");
			sb.append(coordinateX - 0.5);
			sb.append(",");
			sb.append(coordinateY - 0.5);
			sb.append(") {");
			sb.append("..."); // hier Punkte
			sb.append("}; \n");

			// Unterer Block
			if (!farbe.equals("")) {
				sb.append("\\fill[");
				sb.append(farbe);
				sb.append("] (");
			} else {
				sb.append("\\draw (");
			}

			sb.append(coordinateX);
			sb.append(",");
			sb.append(coordinateY - 1);
			sb.append(") rectangle (");
			sb.append(coordinateX - 1);
			sb.append(",");
			sb.append(coordinateY - 2);
			sb.append("); \n");

			// Text im unteren Block
			sb.append("\\node at (");
			sb.append(coordinateX - 0.5);
			sb.append(",");
			sb.append(coordinateY - 1.5);
			sb.append(") {");
			sb.append("..."); // hier Punkte
			sb.append("}; \n");

		} else {

			// Oberer Block
			if (!farbe.equals("")) {
				sb.append("\\fill[");
				sb.append(farbe);
				sb.append("!40!white"); // <----
				sb.append("] (");
			} else {
				sb.append("\\draw (");
			}

			sb.append(coordinateX);
			sb.append(",");
			sb.append(coordinateY);
			sb.append(") rectangle (");
			sb.append(coordinateX - 1);
			sb.append(",");
			sb.append(coordinateY - 1);
			sb.append("); \n");

			// Text im oberen Block
			sb.append("\\node at (");
			sb.append(coordinateX - 0.5);
			sb.append(",");
			sb.append(coordinateY - 0.5);
			sb.append(") {");
			sb.append(wert);
			sb.append("}; \n");

			// Unterer Block
			if (!farbe.equals("")) {
				sb.append("\\fill[");
				sb.append(farbe);
				sb.append("!40!white"); // <----
				sb.append("] (");
			} else {
				sb.append("\\draw (");
			}

			sb.append(coordinateX);
			sb.append(",");
			sb.append(coordinateY - 1);
			sb.append(") rectangle (");
			sb.append(coordinateX - 1);
			sb.append(",");
			sb.append(coordinateY - 2);
			sb.append("); \n");

			// Text im unteren Block
			sb.append("\\node at (");
			sb.append(coordinateX - 0.5);
			sb.append(",");
			sb.append(coordinateY - 1.5);
			sb.append(") {");
			sb.append(index);
			sb.append("}; \n");

		}

		try {
			// Write to File
			bw.append(sb.toString());
			bw.flush();

			// sets new Coordinates
			countBlocks++;
			countBlocksTotal++;

			if (countBlocks >= 10) {
				coordinateStartY = coordinateStartY - 4;
				coordinateY = coordinateStartY;
				coordinateX = 10;
				countBlocks = 0;
			} else {
				coordinateX = coordinateX + 1;
			}

			sb.setLength(0);

		} catch (IOException ioe) {
			return 10;
		}
		return 0;
	}

	/**
	 * <p>
	 * Schreibt eine Ueberschrift (headingText) ueber einen neuen Teil an Daten.
	 * </p>
	 * @param headingText - der Text der die Ueberschrift sein soll
	 * @return
	 * <p> 0 - funktioniert </p>
	 * <p> 10 - IOException bei Filwriter bzw. buffereWriter </p>
	 * <p> 11 - File existiert nicht
	 * <p> 12 - HeadingText fehlt bzw. ist leer 
	 * <p> 13 - Text zu lang
	 * 
	 */
	public int drawHeading(String headingText) {
		
		double firstCoor = 2;
		double secondCoor = 2;
		
		fileTooBig();
		
		if(!writeable) {
			return 11;
		}
		
		if(headingText.isEmpty()) {
			return 12;
		}
		
		if(headingText.length() > 45 ) {
			return 13;
		}
		
		if(headingText.length() > 20 ) {
			firstCoor = firstCoor + 0.5;
			secondCoor = secondCoor + 0.5;
		}
		
		if(headingText.length() > 25 ) {
			firstCoor = firstCoor + 0.5;
			secondCoor = secondCoor + 0.5;
		}
		
		if(headingText.length() > 30 ) {
			firstCoor = firstCoor + 0.5;
			secondCoor = secondCoor + 0.5;
		}
		
		if(headingText.length() > 35 ) {
			firstCoor = firstCoor + 0.5;
			secondCoor = secondCoor + 0.5;
		}
		
		if(headingText.length() > 40 ) {
			firstCoor = firstCoor + 0.5;
			secondCoor = secondCoor + 0.5;
		}
		
		newDraw(4);

		// \draw (StartX + 8, StartY) rectangle (StartX +2, StartY - 0.5);
		// \node at (StartX + 5, StartY - 0,25);

		sb.append("\\draw (");
		sb.append(coordinateStartX + 4 + firstCoor);
		sb.append(",");
		sb.append(coordinateStartY);
		sb.append(") rectangle (");
		sb.append(coordinateStartX + 4 - secondCoor);
		sb.append(",");
		sb.append(coordinateStartY - 0.5);
		sb.append("); \n");

		sb.append("\\node at (");
		sb.append(coordinateStartX + 4);
		sb.append(",");
		sb.append(coordinateStartY - 0.25);
		sb.append(") {");
		sb.append(headingText);
		sb.append("}; \n");

		try {
			// Write to File
			bw.append(sb.toString());
			bw.flush();

			sb.setLength(0);

		} catch (IOException ioe) {
			return 10;
		}		
		
		return 0;

	}

	/**
	 * <p>
	 * Schreibt eine UnterUeberschrift ueber einen Teil der Daten, 
	 * mit dem boolean heading wird eingestellt ob es bereits eine normale Ueberschrift 
	 * gibt oder die UnterUeberschrift alleinstehend soll.
	 * </p>
	 * 
	 * @param subHeadingText -  der Text der die Ueberschrift sein soll
	 * @param heading - ob es eine Ueberschrift bereits gibt oder Subheading alleine steht
	 * @return
	 * <p> 0 - funktioniert </p>
	 * <p> 10 - IOException bei Filwriter bzw. buffereWriter </p>
	 * <p> 11 - File existiert nicht
	 * <p> 12 - HeadingText fehlt bzw. ist leer 
	 * <p> 13 - Text zu lang
	 */
	public int drawSubHeading(String subHeadingText, boolean heading) {
		
		double firstCoor = 1;
		double secondCoor = 1;
		
		fileTooBig();
		
		if(!writeable) {
			return 11;
		}
		
		if (subHeadingText.isEmpty()) {
			return 12;
		}
		
		if(subHeadingText.length() > 35 ) {
			return 13;
		}
		
		if(heading) {
			newDraw(1);
		} else newDraw(2);	
		
		if(subHeadingText.length() > 10 ) {
			firstCoor = firstCoor + 0.5;
			secondCoor = secondCoor + 0.5;
		}
		
		if(subHeadingText.length() > 15 ) {
			firstCoor = firstCoor + 0.5;
			secondCoor = secondCoor + 0.5;
		}
		
		if(subHeadingText.length() > 20 ) {
			firstCoor = firstCoor + 0.5;
			secondCoor = secondCoor + 0.5;
		}
		
		if(subHeadingText.length() > 25 ) {
			firstCoor = firstCoor + 0.5;
			secondCoor = secondCoor + 0.5;
		}
		
		if(subHeadingText.length() > 30 ) {
			firstCoor = firstCoor + 0.5;
			secondCoor = secondCoor + 0.5;
		}
			
		sb.append("\\draw (");
		sb.append(coordinateStartX + 4 + firstCoor);
		sb.append(",");
		sb.append(coordinateStartY);
		sb.append(") rectangle (");
		sb.append(coordinateStartX + 4 - secondCoor);
		sb.append(",");
		sb.append(coordinateStartY - 0.5);
		sb.append("); \n");

		sb.append("\\node at (");
		sb.append(coordinateStartX + 4);
		sb.append(",");
		sb.append(coordinateStartY - 0.25);
		sb.append(") {");
		sb.append(subHeadingText);
		sb.append("}; \n");
		
		try {
			// Write to File
			bw.append(sb.toString());
			bw.flush();

			sb.setLength(0);

		} catch (IOException ioe) {
			return 10;
		}
		return 0;
	}

	/**
	 * <p>
	 * Malt eine Legende fuer die Uebgergebenen Parameter, also die innere Darstellung der Queue.
	 * </p>
	 * 
	 * @param zeigerAnfang - der AnfangsZeiger der Queue
	 * @param zeigerFrei - der FreiZeiger der Queue
	 * @param groesse - Anzahl an Elementen in der Queue
	 * @param speicherGroesse - Anzahl an Elementen die in die Queue passen
	 * @return
	 * <p> 0 - funktioniert </p>
	 * <p> 10 - IOException bei Filwriter bzw. buffereWriter </p>
	 * <p> 11 - File existiert nicht </p>
	 */
	public int drawExplanation(int zeigerAnfang, int zeigerFrei, int groesse, int speicherGroesse) {
		
		if(!writeable) {
			return 11;
		}
		
		
		double coordinateX =  20;
		double coordinateY = coordinateStartY + 1;
		
		
		sb.append("\\draw (");
		sb.append(coordinateX);
		sb.append(",");
		sb.append(coordinateY);
		sb.append(") rectangle (");
		sb.append(coordinateX + 4);
		sb.append(",");
		sb.append(coordinateY -2);
		sb.append("); \n");
		
		sb.append("\\node at(");
		sb.append(coordinateX + 2);
		sb.append(",");
		sb.append(coordinateY - 0.25);
		sb.append(") {");
		sb.append(" Zeiger Anfang : ");
		sb.append(zeigerAnfang);
		sb.append("}; \n");
		
		sb.append("\\node at(");
		sb.append(coordinateX + 2);
		sb.append(",");
		sb.append(coordinateY - 0.75);
		sb.append(") {");
		sb.append(" Zeiger Frei : ");
		sb.append(zeigerFrei);
		sb.append("}; \n");
		
		sb.append("\\node at(");
		sb.append(coordinateX + 2);
		sb.append(",");
		sb.append(coordinateY - 1.25);
		sb.append(") {");
		sb.append(" Size : ");
		sb.append(groesse);
		sb.append("}; \n");
		
		sb.append("\\node at(");
		sb.append(coordinateX + 2);
		sb.append(",");
		sb.append(coordinateY - 1.75);
		sb.append(") {");
		sb.append(" Speicher Groesse : ");
		sb.append(speicherGroesse);
		sb.append("}; \n");
		
		try {
			// Write to File
			bw.append(sb.toString());
			bw.flush();

			sb.setLength(0);

		} catch (IOException ioe) {
			return 10;
		}
		
		return 0;
	}
	
	/**
	 * <p>
	 * Malt eine erweiterte Legende um die farbige Teile in der Abbildung zu erklaeren, maximal koennen 5 Farben erklaert werden
	 * </p>
	 * @param extendedExplanation - Array mit [0] eine Ueberschrift, [1,3,5...] farben, [2,4,6...] beschreibungen
	 * @return
	 * <p> 0 - funktioniert </p>
	 * <p> 10 - IOException bei Filwriter bzw. buffereWriter </p>
	 * <p> 11 - File existiert nicht </p>
	 * <p> 14 - extendedExplanation zu lang </p>
	 * <p> 15 - extendedExplanation ungueltig </p>
	 */
	public int drawExtendExplanation(String[] extendedExplanation) {
		
		boolean ueberschrift = false;
		boolean[] zeilen = {false, false ,false ,false, false};
		
		
		if(!writeable) {
			return 11;
		}
		
		double coordinateX =  20;
		double coordinateY = coordinateStartY - 1;
		
		double coordinateX2 = coordinateX + 4;
		double coordinateY2 = coordinateY;
		
		if(extendedExplanation.length > 11) {
			return 14;
		}
		
		if(!(extendedExplanation.length == 1 || extendedExplanation.length % 2 == 1)) {
			return 15;
		}
		
		if(extendedExplanation[0].length() <= 21 && extendedExplanation[0].length() > 0) {
			ueberschrift = true;
			coordinateY2 = coordinateY2 - 0.5;
		}
		
		
		int counter = 0;
		for(int i = 1; i < extendedExplanation.length; i = i + 2) {
			if((extendedExplanation[i].length() <= 10 && extendedExplanation[i].length() > 0) && (extendedExplanation[i+1].length() <= 10 && extendedExplanation[i+1].length() > 0 )) {
				zeilen[counter] = true;
				coordinateY2 = coordinateY2 - 0.5;
			}
			counter++;
		}
		
		if (!(ueberschrift || zeilen[0] || zeilen[1] || zeilen[2] || zeilen[3] || zeilen[4])) {
			return 15;
		}
		
		// Zeichnet den Kasten drumherum 
		
		sb.append("\\draw (");
		sb.append(coordinateX);
		sb.append(",");
		sb.append(coordinateY);
		sb.append(") rectangle (");
		sb.append(coordinateX2);
		sb.append(",");
		sb.append(coordinateY2);
		sb.append("); \n");
		
		// Zeichnet die Nodes
		
		coordinateX2 = coordinateX + 2;
		coordinateY2 = coordinateY - 0.25;
		
		if(ueberschrift) {			
			sb.append("\\node at(");
			sb.append(coordinateX2);
			sb.append(",");
			sb.append(coordinateY2);
			sb.append(") {");
			sb.append(extendedExplanation[0]);
			sb.append("}; \n");
			coordinateY2 = coordinateY2 - 0.5;
		}
		
		counter = 0;
		for(int i = 1; i < extendedExplanation.length; i = i + 2) {
			if(zeilen[counter]) {
				sb.append("\\node at(");
				sb.append(coordinateX2);
				sb.append(",");
				sb.append(coordinateY2);
				sb.append(") {");
				sb.append(extendedExplanation[i] + " : ");
				sb.append(extendedExplanation[i + 1]);
				sb.append("}; \n");
				coordinateY2 = coordinateY2 - 0.5;
			}
			counter++;
		}
		
		try {
			// Write to File
			bw.append(sb.toString());
			bw.flush();

			sb.setLength(0);

		} catch (IOException ioe) {
			return 10;
		}
		
		return 0;
	}

	/**
	 * <p>
	 * Diese Methode wird aufgerufen, wenn ein Absatz sinnvoll ist bzw. am Anfang
	 * eines neuen Datensatzes.
	 * </p>
	 * 
	 * @param ySub - Wert wie weit die Y Coordinate nach unter verschoben werden soll
	 */
	public void newDraw(int ySub) {
		indexFiles++;
		coordinateStartY = coordinateStartY - ySub;
		countBlocks = 0;
		coordinateStartX = 10;
		coordinateX = 10;
		coordinateY = coordinateStartY;
	}

	/**
	 * <p>
	 * Diese Methode erstellt automatisch eine neue Datei falls die vorherige zu gross wird. 
	 * Die neue Datei hat einen index der sich erhoet.
	 * </p>
	 */
	public void fileTooBig() {
		if(coordinateStartY < -450) {
			indexFiles++;
			endFile();
			startFile(filename, indexFiles);
		} 
	}
	
	public int endFile() {
		if (!writeable) {
			return 11;
		}

		try {
			bw.append("\\end {tikzpicture}");
			bw.flush();
			fileWriter.close();
		} catch (IOException ioe) {
			return 10;
		}
		
		writeable = false;
		return 0;
	}
}
