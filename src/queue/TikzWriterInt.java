package queue;

/**
 * 
 * @author Oliver Kamrath
 *
 * @param <E> Datentyp welcher geprinted werden soll
 */
public interface TikzWriterInt<E extends Comparable<E>> {
	
	/**
	 * <p>
	 * Diese Methode muss als erstes aufgerufen werden und immer beim erstellen einer neuen Datei.
	 * </p>
	 * <p>
	 * Erstellt einen neue Datei mit filename als Namen, und setzt die start Parameter.
	 * Ausserdem werden alle noetigen Startwerte gesetzt und es wird die erste linie an tikzCode
	 * in die Datei geschrieben.
	 * </p>
	 * <p>
	 * Wenn es zu einer IOException kommt wird 10 zurueckegegeben,
	 * andernfalls 0.
	 * </p>
	 * <p>
	 * Setzt das writable Flag auf true.
	 * </p>
	 * 
	 * 
	 * @param filename - Name der zu erstellende Datei
	 * @param indexFiles - Fuer mehere Dateien, falls eine nicht ausreicht (automatisch)
	 * @return
	 * <p> 0 - funktioniert </p>
	 * <p> 9 - File bereits erstellt, aber nicht beendet </p>
	 * <p> 10 - IOException bei Filwriter bzw. buffereWriter </p>
	 */
	int startFile(String filename, int indexFiles);
	
	/**
	 * <p>
	 * Zeichnet einen Arrow an einen block, mit dem uebergebenen Text angeheftet.
	 * </p>
	 * <p>
	 * Wenn die Methode startFile() nocht nicht aufgerufen wurde, oder die Methode endFile() davor, wird 11 
	 * zurueckgeben.
	 * </p>
	 * <p>
	 * Wenn es zu einer IOException kommt wird ebenfalls 10 zurueckgegeben.
	 * </p>
	 * <p>
	 * Andernfalls 0, wenn der Pfeil wie geplant gezeichnet werden konnte.
	 * </p>
	 * 
	 * @param block - Block an den der Pfeil gezeichnet werden soll ausgehend vom ersten
	 * @param text - text der an den Pfeil geschrieben werden soll
	 * @return
	 * <p> 0 - funktioniert </p>
	 * <p> 10 - IOException bei Filwriter bzw. buffereWriter </p>
	 * <p> 11 - File noch nicht erstellt </p>
	 */
	int drawArrow(int block, String text);
	
	/**
	 * <p>
	 * Setzt die Werte in drawNewBlock so, dass ein leerer Block mit Punkten
	 * anstelle von Werten um anzuzeigen, dass etwas ueberstrungen wurde.
	 * </p>
	 * 
	 * @return
	 * <p> 0 - funktioniert </p>
	 * <p> 10 - IOException bei Filwriter bzw. buffereWriter </p>
	 * <p> 11 - File noch nicht erstellt </p>
	 */
	int drawDots();
	
	/**
	 * <p>
	 * Zeichnet einen neune Block mit einem Wert im oberen Teil und einem Index im Unterem.
	 * Optional mit wird dieser einer Farbe markiert.
	 * </p>
	 * <p>
	 * Falls die Methode startFile() noch nicht aufgerufen wird, wird 11 zurueckgegeben.
	 * Ebenso wenn die Methode endFile() aufgerufen wurde.
	 * Falls es zu einer IOException kommt wird ebenfalls 10 zurueckgegeben.
	 * </p>
	 * <p>
	 * Andernfalls 0, wenn der block in die Datei geschrieben werden konnte.
	 * </p>
	 * <p>
	 * Der String in Farbe muss eine nach tikzCode gueltige Farbe in englisch sein.
	 * </p>
	 * 
	 * @param wert - wird in den Oberen Teil des Blocks geschrieben
	 * @param index - wird in den Unteren Teil des Blocks geschrieben
	 * @param farbe - farbe den der Block haben soll, leer wenn keine Farbe
	 * @return
	 * <p> 0 - funktioniert </p>
	 * <p> 10 - IOException bei Filwriter bzw. buffereWriter </p>
	 * <p> 11 - File noch nicht erstellt </p>
	 */
	int drawNewBlock(E wert, int index, String farbe);
	
	/**
	 * <p>
	 * Diese Methode muss am Ende einer Datei aufegrufen werden.
	 * </p>
	 * <p>
	 * Sie setzt die letzte Linie an tikzCode in der Datei.
	 * </p>
	 * <p>
	 * Wenn es zu einer IOException kommt wird 10 zurueckgegeben, 
	 * andernfalls 0.
	 * </p>
	 * <p>
	 * Setzt das writable Flag auf false.
	 * </p>
	 * 
	 * @return
	 * <p> 0 - funktioniert </p>
	 * <p> 10 - IOException bei Filwriter bzw. buffereWriter </p>
	 * <p> 11 - File noch nicht erstellt </p>
	 */
	int endFile();
}
