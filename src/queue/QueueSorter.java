package queue;
import java.util.ArrayList;

/**
 * Erweitert die UtilityQueue um eine Sortier Funktion nach dem ShakerSort 
 * Algorithmus. Ausserdem bietet sie maeglichkeiten um einzelne Schritte dieses Algorithmuses zu speichern
 * und in einer andere Klasse weiterzuverarbteiten.
 * 
 * @author Oliver Kamrath
 *
 * @param <E> - zu speicherder Datentyp
 */
public class QueueSorter<E extends Comparable<E>> extends QueueUtility<E>{
	
	/**
	 * Speichert alle Indexe die in der ersten Schleife (aufsteigend von index 0 zu size) vorkommen. 
	 * Diesen werden durch eine -1 vom naechsten durchlauf abgerenzt.
	 */
	ArrayList<Integer> changedIndexAufsteigend = new ArrayList<Integer>();
	/**
	 * Speichert alle Indexe die in der zweiten Schleife (absteigend von size zu index 0) vorkommen. 
	 * Diesen werden durch eine -1 vom naechsten durchlauf abgerenzt.
	 */
	ArrayList<Integer> changedIndexAbsteigend = new ArrayList<Integer>();
	/**
	 * Speichert alle Zwischenergebnisse nach einem Schleifendurchlauf ab.
	 * 
	 */
	ArrayList<E[]> speicherListe = new ArrayList<E[]>();
	
	/**
	 * Konstruktor des Objekts. 
	 * Speichergroesse muss groesser 0 und kleiner 100 sein, ansonsten wird eine 
	 * IllegalArgumentException geworfen.
	 * @param speicherGroesse - Groesse des Speichers der Queue
	 */
	public QueueSorter(int speicherGroesse) {
		super(speicherGroesse);
	}
	
	/**
	 * <p>
	 * Sortiert die Queue von dem kleinsten zu dem groessten Element. 
	 * Basierend auf dem Shakersort Algorithmus. 
	 * Dazu werden MetaDaten gespeichert, die die einzelen Schritte beinhalten. 
	 * </p>
	 * 
	 * @return 
	 * <p> 0 - funktioniert </p>
	 * <p> 8 - funktioniert aber die Queue is leer </p>
	 * <p> 1 - Speicher falsche groesse (critical) </p>
	 * <p> 2 - ZeigerAnfang falsch (critical) </p>
	 * <p> 3 - ZeigerFrei falsch (critical) </p>
	 * 
	 */
	public int shakerSort() {
		
		if(this.isEmpty()) {
			return 8;
		}
		
		changedIndexAufsteigend.clear();
		changedIndexAbsteigend.clear();
		speicherListe.clear();

		@SuppressWarnings("unchecked")
		E[] newSpeicher = (E[]) new Comparable[this.getSpeicherSize()];
		E zwischenSpeicher = null;
		

		// lade Queue in neuenSpeicher
		for (int i = 0; i < this.size(); i++) {
			newSpeicher[i] = this.getElement(i);
		}
		
		speicherListe.add(newSpeicher.clone());

		int anfang = 0;
		// int ende = this.size() - 2;
		int ende = this.size() - 1;
		boolean vertauscht = true;

		while (vertauscht) {
			vertauscht = false;
			
			// von vorne
			for (int index = anfang; index < ende; index++) {
				if (newSpeicher[index].compareTo(newSpeicher[index + 1]) > 0) {
					zwischenSpeicher = newSpeicher[index];
					newSpeicher[index] = newSpeicher[index + 1];
					newSpeicher[index + 1] = zwischenSpeicher;
					vertauscht = true;
					changedIndexAufsteigend.add(index);
				}
			}
			changedIndexAufsteigend.add(-1);
			
			if(!vertauscht) {
				break;
			}

			// von hinten
			for (int index = ende; index > anfang; index--) {
				if (newSpeicher[index].compareTo(newSpeicher[index - 1]) < 0) {
					zwischenSpeicher = newSpeicher[index];
					newSpeicher[index] = newSpeicher[index - 1];
					newSpeicher[index - 1] = zwischenSpeicher;
					vertauscht = true;
					changedIndexAbsteigend.add(index);
				}
			}
			changedIndexAbsteigend.add(-1);
			ende--;
			anfang++;
			speicherListe.add(newSpeicher.clone());
		}
		
		int zeigerFrei = this.size();
		if (zeigerFrei == this.getSpeicherSize()) {
			zeigerFrei = 0;
		}

		return this.setSpeicher(newSpeicher, 0, zeigerFrei);
	}

	/**
	 * Gibt die Liste mit allen Indexen aus der ersten Schleife zurueck. 
	 * Muss erst nach shakerSort() aufgerufen werden, da diese andernfalls leer ist.
	 * @return Liste mit allen Indexen
	 */
	public ArrayList<Integer> getChangedIndexAufsteigend() {
		return changedIndexAufsteigend;
	}
	
	/**
	 * Gibt die Liste mit allen Indexen aus der zweiten Schleife zurueck. 	
	 * Muss erst nach shakerSort() aufgerufen werden, da diese andernfalls leer ist.
	 * @return Liste mit allen Indexen
	 */
	public ArrayList<Integer> getChangedIndexAbsteigend() {
		return changedIndexAbsteigend;
	}
	
	/**
	 * Gibt eine Liste mit allen ZwischenSpeicher ergebnissen nach jeden Schleifendurchlauf zurueck.
	 * @return List mit allen Zwischespeichern
	 */
	public ArrayList<E[]> getNewSpeicherListe() {
		return speicherListe;
	}
}
