package queue;

import java.util.ArrayList;


/**
 * 
 * <p>
 * Diese Klasse kann Queue als Tikz Datei darstellen. Es gibt diverse Methoden
 * um die Darstellung anzupassen, welche auf den Klassen des TikzWriters
 * basieren. Jede Darstellung der Queue kann in einer eigenen Datei stehen, aber
 * es koennen auch mehere in der Selben Datei sein. Weiterhin gibt es eine
 * vordefinierte Methode, welche den ShakerSort Algorithmus darstellen kann,
 * sodass man diesen besser nachvollziehen kann.
 * </p>
 * 
 * @author Oliver Kamrath
 *
 * @param <E> Datentyp welcher in der Queue gespeichert wird
 */
public class QueuePrinter<E extends Comparable<E>> extends TikzWriter<E> {

	/**
	 * <p>
	 * Gibt an welche Teile der Queue ausgegeben werden sollen.
	 * </p>
	 * <p>
	 * parts[0] = AnfangsIndex
	 * </p>
	 * <p>
	 * parts[1] = endIndex
	 * </p>
	 * <p>
	 * parts[3] = mitteAnfang
	 * </p>
	 * <p>
	 * parts[4] = mitteEnde
	 * </p>
	 */
	private int[] parts;
	/**
	 * Gibt an ob nur bestimmte Teile der Queue ausgegeben werden sollen.
	 */
	private boolean partsSet;
	/**
	 * Die Ueberschrift der zu printenden Queue.
	 */
	private String heading;
	/**
	 * Gibt an ob eine Ueberschrift gesetzt wurde.
	 */
	private boolean headingSet;
	/**
	 * Die UnterUeberschrift der zu printenden Queue.
	 */
	private String subHeading;
	/**
	 * Gibt an ob eine UnterUberschrift gesetzt wurde.
	 */
	private boolean subHeadingSet;
	/**
	 * Die indexe die Farbig markiert werden sollen. Gleich gross wie der
	 * dazugerhoerige farb String. 1 index zu 1 farbe.
	 */
	private int[] indexe;
	/**
	 * Gibt an welche Farben an welchen index sein sollen. Farben in englisch und
	 * tikz gueltig.
	 */
	private String[] farbe;
	/**
	 * Gibt an ob farben gesetzt werden sollen.
	 */
	private boolean farbeSet;
	/**
	 * Gibt an ob eine Eplanation gezeichnet werden soll. Beinhaltet Zeiger, groesse
	 * etc.
	 */
	private boolean explanation;
	/**
	 * Speichert die beschreibung fuer die extended Explanation. [0] ueberschrift,
	 * [1,3,5...] farben, [2,4,6...] Beschreibung.
	 */
	private String[] extendedExplanation;
	/**
	 * Gibt an ob die erweiterte Beschreibung gezeichnet werden soll.
	 */
	private boolean extendedEplanationSet;
	/**
	 * Die Queu fuer welche alles gezeichnet werden soll.
	 */
	protected QueueSorter<E> qu;

	/**
	 * Der Konstruktor, setzt alle extra zeichen Werte auf 0 und bekommt eine Queue
	 * zu welcher das TikzDokument gemalt werden soll.
	 * 
	 * @param qu - uebergebene Queue
	 */
	public QueuePrinter(QueueSorter<E> qu) {
		this.qu = qu;
		this.partsSet = false;
		this.headingSet = false;
		this.subHeadingSet = false;
		this.farbeSet = false;
		this.explanation = false;
		this.extendedEplanationSet = false;
	}

	/**
	 * <p>
	 * Methode um einzustellen, dass nur bestimmte Teile der queue angezeigt werden.
	 * </p>
	 * <p>
	 * Bekommt ein int Array nach dem folgenden musster:
	 * </p>
	 * <p>
	 * parts[0] = AnfangsIndex
	 * </p>
	 * <p>
	 * parts[1] = endIndex
	 * </p>
	 * <p>
	 * parts[3] = mitteAnfang
	 * </p>
	 * <p>
	 * parts[4] = mitteEnde
	 * </p>
	 * <p>
	 * Wenn eine Part nicht genutzt werden soll, dann soll dieser einfach auf 0
	 * gesetzt werden.
	 * </p>
	 * 
	 * @param parts - array das Beschreibt welche Teile der queue Angezeigt werden
	 *              sollen
	 * @return
	 *         <p>
	 *         0 - funktioniert
	 *         </p>
	 *         <p>
	 *         20 - nicht gesetzt Array ungueltige Groe�e
	 *         </p>
	 *         <p>
	 *         21 - ein/ mehere Parts falsch eingegeben, trotzdem gesetzt
	 *         </p>
	 *         <p>
	 *         23 - null/ parts auf false gesetzt
	 *         </p>
	 */
	public int setParts(int[] parts) {
		int errorCode = 0;

		if (parts == null) {
			partsSet = false;
			return 23;
		}

		if (parts.length != 4) {
			partsSet = false;
			return 20;
		}

		if (parts[0] < 0 || parts[0] > qu.size()) {
			parts[0] = 0;
			errorCode = 21;
		}

		if (parts[1] < 0 || parts[1] > qu.size() || parts[1] <= parts[0]) {
			if (parts[1] == 0) {
				parts[1] = qu.size();
			} else {
				parts[1] = qu.size();
				errorCode = 21;
			}
		}

		if (parts[2] != 0) {
			if (parts[2] < parts[0] || parts[2] > parts[1] || parts[2] < 0 || parts[2] > qu.size()) {
				parts[2] = qu.size();
				errorCode = 21;
			} else if (parts[3] == 0) {
				parts[2] = qu.size();
				errorCode = 21;
			}
		} else {

			parts[2] = qu.size();
		}

		if (parts[3] != 0) {
			if (parts[3] < parts[2] || parts[3] > parts[1] || parts[3] < 0 || parts[3] > qu.size()) {
				parts[3] = qu.size();
				errorCode = 21;
			}
		} else {
			parts[3] = qu.size();
		}

		this.parts = parts;
		partsSet = true;
		return errorCode;
	}

	/**
	 * Setzt den ueberschrift Parameter, wenn gueltig wird true zurueckgebene, wenn
	 * nicht false. Dieser wird dann automatisch in die Queue gemalt. Bei heading
	 * sollte ein leerer String eingesetzt werden um diesen auszuschalten.
	 * 
	 * @param heading - die Ueberschrift
	 * @return
	 *         <p>
	 *         true - funktioniert
	 *         </p>
	 *         <p>
	 *         false - ueberschrift leer oder zu lang
	 *         </p>
	 */
	public boolean setHeading(String heading) {
		if (heading.isEmpty()) {
			headingSet = false;
			return headingSet;
		}

		if (heading.length() > 45) {
			headingSet = false;
			return headingSet;
		}
		this.heading = heading;
		headingSet = true;
		return headingSet;
	}

	/**
	 * Setzt den unterUeberschrift Parameter, wenn gueltig wird true zurueckgegeben,
	 * wenn nicht false. Dieser wird dann automatisch in die Queue gemalt. Bei
	 * subHeading sollte ein leerer String eingesetzt werden um diesen
	 * auszuschalten.
	 * 
	 * @param subHeading - die UnterUeberschrift
	 * @return
	 *         <p>
	 *         true - funktioniert
	 *         </p>
	 *         <p>
	 *         false - unterUeberschrift leer oder zu lang
	 *         </p>
	 */
	public boolean setSubHeading(String subHeading) {
		if (subHeading.isEmpty()) {
			subHeadingSet = false;
			return subHeadingSet;
		}
		if (subHeading.length() > 35) {
			subHeadingSet = false;
			return subHeadingSet;
		}
		this.subHeading = subHeading;
		subHeadingSet = true;
		return subHeadingSet;
	}

	/**
	 * <p>
	 * Setzt die Indexe die Farbig werden sollen und die dazugehoerigen farben.
	 * </p>
	 * 
	 * @param indexe - Array mit indexen die Farbig werden sollen
	 * @param farbe  - Array mit Farben in Englisch die an den indexen gemalt werden
	 *               sollen.
	 * @return
	 *         <p>
	 *         true - funktioniert
	 *         </p>
	 *         <p>
	 *         false - index laenge != farbe laenge OR eins von beiden NULL
	 *         </p>
	 */
	public boolean setIndexeFarbe(int[] indexe, String[] farbe) {

		if (indexe == null || farbe == null) {
			farbeSet = false;
			return farbeSet;
		}

		if (indexe.length != farbe.length) {
			farbeSet = false;
			return farbeSet;
		}
		this.indexe = indexe;
		this.farbe = farbe;
		farbeSet = true;
		return farbeSet;
	}

	/**
	 * <p>
	 * Schaltet die Explanation fuer Groessen und Zeiger Anzeige an und aus.
	 * </p>
	 * 
	 * @param explanation - an/aus
	 * @return
	 *         <p>
	 *         true - eingeschaltet
	 *         </p>
	 *         <p>
	 *         false - ausgeschaltet
	 *         </p>
	 */
	public boolean setExplanation(boolean explanation) {
		this.explanation = explanation;
		return this.explanation;
	}

	/**
	 * <p>
	 * Setzt die Extended Explanation fuer Farben. [0] = Ueberschrift, [1,3,5..]
	 * farben, [2,4,6..] beschreibung
	 * </p>
	 * <p>
	 * Maximal groesse ist 11.
	 * </p>
	 * 
	 * @param extendedExplanation - String array mit der Beschreibung
	 * @return
	 *         <p>
	 *         0 - funktioniert
	 *         </p>
	 *         <p>
	 *         14 - extendedExplanation zu gross
	 *         </p>
	 *         <p>
	 *         15 - extendedExplanation ungueltige groesse
	 *         </p>
	 *         <p>
	 *         23 - null/ extendedExplanation auf false gesetzt
	 *         </p>
	 */
	public int setExtendedExplanation(String[] extendedExplanation) {

		if (extendedExplanation == null) {
			extendedEplanationSet = false;
			return 23;
		}

		if (extendedExplanation.length > 11) {
			extendedEplanationSet = false;
			return 14;
		}

		if (!(extendedExplanation.length == 1 || extendedExplanation.length % 2 == 1)) {
			extendedEplanationSet = false;
			return 15;
		}

		this.extendedExplanation = extendedExplanation;
		extendedEplanationSet = true;
		return 0;
	}

	/**
	 * <p>
	 * Benutzt draw(sub)Heading um die zuvor gesetzten Ueberschriften in die pdf zu
	 * schreiben. Falls keine Ueberschrift getzt wurde passiert nichts.
	 * </p>
	 * 
	 * @return
	 *         <p>
	 *         0 - funktioniert
	 *         </p>
	 *         <p>
	 *         10 - IOException bei Filwriter bzw. buffereWriter
	 *         </p>
	 *         <p>
	 *         11 - File existiert nicht
	 *         </p>
	 *         <p>
	 *         12 - HeadingText fehlt bzw. ist leer
	 *         </p>
	 *         <p>
	 *         13 - Text zu lang
	 *         </p>
	 */
	private int doHeadings() {
		int errorCode = 0;
		if (headingSet) {
			errorCode = this.drawHeading(heading);
			if (errorCode != 0) {
				return errorCode;
			}
		}

		if (subHeadingSet) {
			errorCode = this.drawSubHeading(subHeading, headingSet);
			if (errorCode != 0) {
				return errorCode;
			}
		}

		if (headingSet || subHeadingSet) {
			this.newDraw(2);
		}
		return errorCode;
	}

	/**
	 * <p>
	 * Benutzt drawExplanation und drawExtendedExplanation, um die Erklaerungen fuer
	 * die Queue in die PDF zu zeichenen. Diese sollten vorher mit setExplanation
	 * und setExetendedExplanation gesetzt werden. Falls diese nicht gesetzt wurden
	 * passiert nichts.
	 * </p>
	 * 
	 * @return
	 *         <p>
	 *         0 - funktioniert
	 *         </p>
	 *         <p>
	 *         10 - IOException bei Filwriter bzw. buffereWriter
	 *         </p>
	 *         <p>
	 *         11 - File existiert nicht
	 *         </p>
	 *         <p>
	 *         14 - extendedExplanation zu lang
	 *         </p>
	 *         <p>
	 *         15 - extendedExplanation ungueltig
	 *         </p>
	 */
	private int doExplanations() {
		int errorCode = 0;

		if (explanation) {
			errorCode = this.drawExplanation(qu.getZeigerAnfang(), qu.getZeigerFrei(), qu.size(), qu.getSpeicherSize());
			if (errorCode != 0) {
				return errorCode;
			}
		}

		if (extendedEplanationSet) {
			errorCode = this.drawExtendExplanation(extendedExplanation);
			if (errorCode != 0) {
				return errorCode;
			}
		}

		return errorCode;
	}

	/**
	 * <p>
	 * Printed die queue, uebergeben als Array, in die PDF mithilfe von
	 * drawNewBlock.
	 * </p>
	 * 
	 * @param qu - Array representation der Queue
	 * @return
	 *         <p>
	 *         0 - funktioniert
	 *         </p>
	 *         <p>
	 *         10 - IOException bei Filwriter bzw. buffereWriter
	 *         </p>
	 *         <p>
	 *         11 - File noch nicht erstellt
	 *         </p>
	 */
	private int doQueuePrint(E[] qu) {
		int errorCode = 0;

		for (int index = 0; index < qu.length; index++) {

			errorCode = this.drawNewBlock(qu[index], index, "");
			if (errorCode != 0) {
				return errorCode;
			}
		}
		return errorCode;
	}

	/**
	 * <p>
	 * Printed die queue, uebergeben als Array, in die PDF mithilfe von
	 * drawNewBlock. Zusaetzlich werden mit setIndexeFarben gesetzte Farben
	 * beruecksichtigt. Um diese Methode zu benutzten muessen Farben gesetzt worden
	 * sein.
	 * </p>
	 * 
	 * @param qu - Array representation der Queue
	 * @return
	 *         <p>
	 *         0 - funktioniert
	 *         </p>
	 *         <p>
	 *         10 - IOException bei Filwriter bzw. buffereWriter
	 *         </p>
	 *         <p>
	 *         11 - File noch nicht erstellt
	 *         </p>
	 *         <p>
	 *         22 - falsche Print Methode aufgerufen
	 *         </p>
	 */
	private int doQueuePrintFarben(E[] qu) {

		if (!farbeSet) {
			return 22;
		}

		int errorCode = 0;
		boolean farbStelle = false;
		int counterF = 0;

		for (int index = 0; index < qu.length; index++) {

			for (int iF = 0; iF < indexe.length; iF++) {
				if (indexe[iF] == index) {
					farbStelle = true;
					counterF = iF;
					break;
				}
			}

			if (farbStelle) {
				errorCode = this.drawNewBlock(qu[index], index, farbe[counterF]);
				if (errorCode != 0) {
					return errorCode;
				}
			} else {
				errorCode = this.drawNewBlock(qu[index], index, "");
				if (errorCode != 0) {
					return errorCode;
				}
			}

			farbStelle = false;
		}
		return errorCode;

	}

	/**
	 * <p>
	 * Printed die queue, uebergeben als Array, in die PDF mithilfe von
	 * drawNewBlock. Zusaetzlich werden mit setParts gesetzte Teile beruecksichtigt.
	 * Um diese Methode zu benutzten muss Parts gesetzt worden sein.
	 * </p>
	 * 
	 * @param qu - Array representation der Queue
	 * @return
	 *         <p>
	 *         0 - funktioniert
	 *         </p>
	 *         <p>
	 *         10 - IOException bei Filwriter bzw. buffereWriter
	 *         </p>
	 *         <p>
	 *         11 - File noch nicht erstellt
	 *         </p>
	 *         <p>
	 *         22 - falsche Print Methode aufgerufen
	 *         </p>
	 */
	private int doQueuePrintParts(E[] qu) {
		if (!partsSet) {
			return 22;
		}
		int errorCode = 0;
		boolean lueckePrinted = false;

		for (int index = 0; index < qu.length; index++) {

			if (index >= parts[0] && index <= parts[1] && !(index >= parts[2] && index <= parts[3])) {
				errorCode = this.drawNewBlock(qu[index], index, "");
				if (errorCode != 0) {
					return errorCode;
				}
				lueckePrinted = false;
			} else if (!lueckePrinted) {

				errorCode = this.drawDots();
				if (errorCode != 0) {
					return errorCode;
				}

				lueckePrinted = true;
			}
		}
		return errorCode;

	}

	/**
	 * <p>
	 * Printed die queue, uebergeben als Array, in die PDF mithilfe von
	 * drawNewBlock. Zusaetzlich werden mit setIndexeFarben gesetzte Farben
	 * beruecksichtigt. Um diese Methode zu benutzten muessen Farben gesetzt worden
	 * sein. Ausserdem werden mit setParts gesetzte Teile beruecksichtigt. Um diese
	 * Methode zu benutzten muss Parts gesetzt worden sein.
	 * </p>
	 * 
	 * @param qu - Array representation der Queue
	 * @return
	 *         <p>
	 *         0 - funktioniert
	 *         </p>
	 *         <p>
	 *         10 - IOException bei Filwriter bzw. buffereWriter
	 *         </p>
	 *         <p>
	 *         11 - File noch nicht erstellt
	 *         </p>
	 *         <p>
	 *         22 - falsche Print Methode aufgerufen
	 *         </p>
	 */
	private int doQueuePrintFarbenParts(E[] qu) {
		if (!partsSet) {
			return 22;
		}

		if (!farbeSet) {
			return 22;
		}

		int errorCode = 0;
		boolean lueckePrinted = false;
		boolean farbStelle = false;
		int counterF = 0;

		for (int index = 0; index < qu.length; index++) {

			for (int iF = 0; iF < indexe.length; iF++) {
				if (indexe[iF] == index) {
					farbStelle = true;
					counterF = iF;
					break;
				}
			}

			if (index >= parts[0] && index <= parts[1] && !(index >= parts[2] && index <= parts[3])) {

				if (farbStelle) {
					errorCode = this.drawNewBlock(qu[index], index, farbe[counterF]);
					if (errorCode != 0) {
						return errorCode;
					}
				} else {
					errorCode = this.drawNewBlock(qu[index], index, "");
					if (errorCode != 0) {
						return errorCode;
					}
				}

				lueckePrinted = false;
			} else if (!lueckePrinted) {

				errorCode = this.drawDots();
				if (errorCode != 0) {
					return errorCode;
				}

				lueckePrinted = true;
			}
		}
		return errorCode;
	}

	/**
	 * <p>
	 * Printed eine Queue als TikzDarstellung in eine TikzDatei. Es koennen mit
	 * dieser Methode mehere Queue in die selbe Datei geprinted werden. Nutze die
	 * anderen Methoden dieser Klasse um die Darstellung anzupassen, wie z.B. das
	 * Setzten von Ueberschriften.
	 * </p>
	 * <p>
	 * Bevor diese Methode aufgerufen wird muss startFile() aufgerufen werden, und
	 * wenn man Fertig ist mit dem Printen in die Datei muss endFile() aufgerufen
	 * werden.
	 * </p>
	 * <p>
	 * Vielleicht sollte newDraw() vor jeder neunen Queue gecalled werden
	 * </p>
	 * 
	 * @return
	 *         <p>
	 *         0 - funktioniert
	 *         </p>
	 *         <p>
	 *         10 - IOException bei Filwriter bzw. buffereWriter
	 *         </p>
	 *         <p>
	 *         11 - File existiert nicht
	 *         </p>
	 *         <p>
	 *         12 - (sub)HeadingText fehlt bzw. ist leer
	 *         </p>
	 *         <p>
	 *         13 - (sub)HeadinText zu lang
	 *         </p>
	 *         <p>
	 *         14 - extendedExplanation zu lang
	 *         </p>
	 *         <p>
	 *         15 - extendedExplanation ungueltig
	 *         </p>
	 *         <p>
	 *         22 - falsche Print Methode aufgerufen
	 *         </p>
	 * 
	 */
	public int printQueueToSameFile() {

		int errorCode = 0;
		this.newDraw(2);

		errorCode = doHeadings();
		if (errorCode != 0) {
			return errorCode;
		}

		errorCode = doExplanations();
		if (errorCode != 0) {
			return errorCode;
		}

		E[] speicher = qu.toArray();

		if (!partsSet) {
			if (farbeSet) {
				errorCode = doQueuePrintFarben(speicher);
				if (errorCode != 0) {
					return errorCode;
				}
			} else {
				errorCode = doQueuePrint(speicher);
				if (errorCode != 0) {
					return errorCode;
				}
			}
		} else {
			if (farbeSet) {
				errorCode = doQueuePrintFarbenParts(speicher);
				if (errorCode != 0) {
					return errorCode;
				}
			} else {
				errorCode = doQueuePrintParts(speicher);
				if (errorCode != 0) {
					return errorCode;
				}
			}

		}

		if (qu.size() % 10 == 0) {
			this.newDraw(-4);
		}

		/*
		 * Keine Pfeil mehr, die Anzeigen wo der Anfangs/ FreiZeiger ist, dafuer wird
		 * die Legende benutzt Sie waeren sowieso nur beim ersten und letzten Index zu
		 * sehen, da die Darstellung so gewahelt wurde
		 * 
		 * if (!this.drawArrow(qu.getZeigerAnfang(), "zA")) {
		 * 
		 * return false; } ;
		 * 
		 * if (!this.drawArrow(qu.getZeigerFrei(), "zF")) {
		 * 
		 * return false; } ;
		 * 
		 */

		return errorCode;
	}

	/**
	 * 
	 * *
	 * <p>
	 * Printed eine Queue als TikzDarstellung in eine TikzDatei. Nutze die anderen
	 * Methoden dieser Klasse um die Darstellung anzupassen, wie z.B. das Setzten
	 * von Ueberschriften.
	 * </p>
	 * 
	 * <p>
	 * Vielleicht sollte newDraw() vor jeder neunen Queue gecalled werden
	 * </p>
	 * 
	 * @param filename - Name der Datei in welcher die Queue gespeichert wird
	 * @return
	 *         <p>
	 *         0 - funktioniert
	 *         </p>
	 *         <p>
	 *         9 - File bereits erstellt, aber nicht beendet
	 *         </p>
	 *         <p>
	 *         10 - IOException bei Filwriter bzw. buffereWriter
	 *         </p>
	 *         <p>
	 *         11 - File existiert nicht
	 *         </p>
	 *         <p>
	 *         12 - (sub)HeadingText fehlt bzw. ist leer
	 *         </p>
	 *         <p>
	 *         13 - (sub)HeadingText zu lang
	 *         </p>
	 *         <p>
	 *         14 - extendedExplanation zu lang
	 *         </p>
	 *         <p>
	 *         15 - extendedExplanation ungueltig
	 *         </p>
	 *         <p>
	 *         22 - falsche Print Methode aufgerufen
	 *         </p>
	 */
	public int printQueue(String filename) {

		int errorCode = 0;
		errorCode = this.startFile(filename, 0);

		if (errorCode != 0) {
			return errorCode;
		}

		errorCode = printQueueToSameFile();

		if (errorCode != 0) {
			this.endFile();
			return errorCode;
		}

		errorCode = this.endFile();
		return errorCode;
	}

	/**
	 * 
	 * <p>
	 * Die Methode printed eine Queue auf die der Sortier Algorithmus angewendet
	 * wird. Sie printed jeden einzelnen Schritt welcher zum Darstellen der Sortierung notwengig ist. 
	 * Dies wird in eine eigene Datei geprinted. 
	 * Vor dem aufrufen der Methode muss ShakeSort() aufgerufen werden.
	 * </p>
	 * 
	 * @param filename - Der Name der Datei
	 * @return
	 *         <p>
	 *         0 - funktioniert
	 *         </p>
	 *         <p>
	 *         1 - Speicher falsche groesse (critical)
	 *         </p>
	 *         <p>
	 *         2 - ZeigerAnfang falsch (critical)
	 *         </p>
	 *         <p>
	 *         3 - ZeigerFrei falsch (critical)
	 *         </p>
	 *         <p>
	 *         8 - Queue leer
	 *         </p>
	 *         <p>
	 *         9 - File bereits erstellt, aber nicht beendet
	 *         </p>
	 *         <p>
	 *         10 - IOException bei Filwriter bzw. buffereWriter
	 *         </p>
	 *         <p>
	 *         11 - File existiert nicht
	 *         </p>
	 *         <p>
	 *         12 - HeadingText fehlt bzw. ist leer
	 *         </p>
	 *         <p>
	 *         13 - Text zu lang
	 *         </p>
	 *         <p>
	 *         14 - extendedExplanation zu lang
	 *         </p>
	 *         <p>
	 *         15 - extendedExplanation ungueltig
	 *         </p>
	 *         <p>
	 *         12 - (sub)HeadingText fehlt bzw. ist leer
	 *         </p>
	 *         <p>
	 *         13 - (sub)HeadinText zu lang
	 *         </p>
	 *         <p>
	 *         14 - explanation too long
	 *         </p>
	 *         <p>
	 *         15 - extendedExplanation ungueltig
	 *         </p>
	 *         <p>
	 *         16 - setIndexFarben error
	 *         </p>
	 *         <p>
	 *         17 - Ueberschrift error
	 *         </p>
	 *         <p>
	 *         22 - falsche Print Methode aufgerufen
	 *         </p>
	 *         <p>
	 *         23 - null/ parts auf false gesetzt
	 *         </p>
	 *         <p>
	 *         24 - Shake Sort noch nicht ausgefuehrt
	 *         </p>
	 */
	public int printQueueAlgorithm(String filename) {
		int errorCode = 0;

		this.startFile(filename, 0);

		if (qu.getNewSpeicherListe().isEmpty()) {
			return 24;
		}

		// Get Meta Daten
		ArrayList<E[]> speicherAQ = qu.getNewSpeicherListe();
		ArrayList<Integer> indexAufsteigendList = qu.getChangedIndexAufsteigend();
		ArrayList<Integer> indexAbsteigendList = qu.getChangedIndexAbsteigend();

		// Alle Attribute auf False

		setHeading("");
		setSubHeading("");
		setParts(null);
		setExplanation(false);
		setExtendedExplanation(null);
		setIndexeFarbe(null, null);

		/*
		 * fuer alle Zustaende
		 * 
		 * 1. ein zusand bekommen
		 * 
		 * 2. diesen Zustand nach oben printen bis der index getauscht wird 3. dann
		 * index tauschen in seperatem print 4. zustand weiter nach oben printen
		 * 
		 * 5. result anzeigen von nach oben print
		 * 
		 * 6. zustand nach unten printen bis der index getauscht wird 7. dann index
		 * tauschen in seperatem print 8. zustand weiter nach unten printen
		 * 
		 * 7. result printen
		 *
		 * 
		 */

		// Variablen zum Printen

		int indexListeAufsteigend = 0;
		int indexListeAbsteigend = 0;
		int anzahlTausch = 0;
		int anzahlTauschTotal = 0;
		E zwischenSpeicher;
		int schleifendurchlauf = 0;

		// Print Queue Davor
		errorCode = printSortQueueResult(speicherAQ.get(0), 0, 0, 0);
		if (errorCode != 0) {
			return errorCode;
		}

		// Variablen von ShakeSort
		int anfang = 0;
		int ende = qu.size() - 1;
		boolean vertauscht;

		for (E[] speicher : speicherAQ) {

			vertauscht = false;
			schleifendurchlauf++;

			for (int index = anfang; index < ende; index++) {

				errorCode = printSortQueueSchleife(speicher, index, true, false, schleifendurchlauf, anfang, ende);

				if (errorCode != 0) {
					return errorCode;
				}

				if (testIndex(indexAufsteigendList, indexListeAufsteigend, index)) {

					indexListeAufsteigend++;

					// Tausch
					zwischenSpeicher = speicher[index];
					speicher[index] = speicher[index + 1];
					speicher[index + 1] = zwischenSpeicher;
					vertauscht = true;

					anzahlTausch++;
					anzahlTauschTotal++;

					errorCode = printSortQueueSchleife(speicher, index, true, true, schleifendurchlauf, 0, 0);

					if (errorCode != 0) {
						return errorCode;
					}

				}

			}

			indexListeAufsteigend++;

			// print Queue nach schleife oben
			errorCode = printSortQueueResult(speicher, 1, schleifendurchlauf, anzahlTausch);
			if (errorCode != 0) {
				return errorCode;
			}
			anzahlTausch = 0;

			if (vertauscht == false) {
				break;
			}

			for (int index = ende; index > anfang; index--) {

				errorCode = printSortQueueSchleife(speicher, index, false, false, schleifendurchlauf, ende, anfang);

				if (errorCode != 0) {
					return errorCode;
				}

				if (testIndex(indexAbsteigendList, indexListeAbsteigend, index)) {

					indexListeAbsteigend++;

					// Tausch
					zwischenSpeicher = speicher[index];
					speicher[index] = speicher[index - 1];
					speicher[index - 1] = zwischenSpeicher;

					anzahlTausch++;
					anzahlTauschTotal++;

					errorCode = printSortQueueSchleife(speicher, index, false, true, schleifendurchlauf, 0, 0);

					if (errorCode != 0) {
						return errorCode;
					}

				}

			}
			indexListeAbsteigend++;

			// print Queue nach schleife unten
			errorCode = printSortQueueResult(speicher, 2, schleifendurchlauf, anzahlTausch);
			if (errorCode != 0) {
				return errorCode;
			}
			anzahlTausch = 0;

			ende--;
			anfang++;

		}

		// Print Queue Danach
		errorCode = printSortQueueResult(speicherAQ.get(speicherAQ.size() - 1), 3, schleifendurchlauf,
				anzahlTauschTotal);
		if (errorCode != 0) {
			return errorCode;
		}
		
		errorCode = this.endFile();
		if(errorCode != 0) {
			return errorCode;
		}

		return errorCode;
	}

	/**
	 * <p>
	 * Hilfsmethode fuer printQueueAlgorithm. Printed die Queue in eine Tikz Datei.
	 * Wird aufgerufen vor und nachdem Darstellen des Algorithmus, alsauch nach
	 * jedem Schleifendurchlauf. Die Ort Variable beschreibt dies. Die
	 * Ueberschriften werden entsprechend angepasst.
	 * </p>
	 * 
	 * @param speicher           - Die zu printende Queue als Array dargestellt
	 * @param ort                - der Ort wo die Methode aufgerufen wird 0-3
	 *                           gueltig
	 * @param schleifenDurchlauf - die Anazahl an Scheleifendurchlaeufen bis zum
	 *                           Aufrufen der Methode
	 * @param anzahlTausche      - die Anzahl an Tauschen die bist zum aufruf der
	 *                           Methoe stattgefunden hat
	 * @return
	 *         <p>
	 *         0 - funktioniert
	 *         </p>
	 *         <p>
	 *         10 - IOException bei Filwriter bzw. buffereWriter
	 *         </p>
	 *         <p>
	 *         11 - File noch nicht erstellt
	 *         </p>
	 *         <p>
	 *         12 - (sub)HeadingText fehlt bzw. ist leer
	 *         </p>
	 *         <p>
	 *         13 - (sub)HeadinText zu lang
	 *         </p>
	 *         <p>
	 *         14 - explanation too long
	 *         </p>
	 *         <p>
	 *         15 - extendedExplanation ungueltig
	 *         </p>
	 *         <p>
	 *         17 - Ueberschrift error
	 *         </p>
	 *         <p>
	 *         23 - null/ parts auf false gesetzt
	 *         </p>
	 * 
	 */
	private int printSortQueueResult(E[] speicher, int ort, int schleifenDurchlauf, int anzahlTausche) {
		int errorCode = 0;

		if (ort < 0 || ort > 3) {
			return 18;
		}

		String ueberschrift;
		String subUeberschrift;

		if (ort == 0) {
			ueberschrift = "Queue unsortiert vor ShakeSort";
			subUeberschrift = "";
		} else if (ort == 3) {
			ueberschrift = "Queue sortiert nach ShakeSort";
			subUeberschrift = "";
		} else if (ort == 1) {
			ueberschrift = "Queue nach der " + schleifenDurchlauf + " Schleife nach rechts";
			subUeberschrift = "Es wurden: " + anzahlTausche + " Elemente getauscht";
		} else {
			ueberschrift = "Queue nach der " + schleifenDurchlauf + " Schleife nach links";
			subUeberschrift = "Es wurden: " + anzahlTausche + " Elemente getauscht";
		}

		// Explanation an
		this.setExplanation(true);

		// Ueberschrift mit art der Schleife
		if (!this.setHeading(ueberschrift)) {
			return 17;
		}

		if (ort == 3) {
			// Explanation fuer Farbe an Index
			errorCode = this.setExtendedExplanation(new String[] { "Daten", "Tausche", Integer.toString(anzahlTausche),
					"durchlaufe", Integer.toString(schleifenDurchlauf) });

			if (errorCode != 0) {
				return errorCode;
			}
		}

		// UnterUeberschrift mit SchleifenDurchlauf
		this.setSubHeading(subUeberschrift);

		// Do Ueberschrift
		errorCode = this.doHeadings();
		if (errorCode != 0) {
			return errorCode;
		}

		// DO Explanation
		errorCode = this.doExplanations();
		if (errorCode != 0) {
			return errorCode;
		}

		// Draw Queue
		errorCode = doQueuePrint(speicher);
		if (errorCode != 0) {
			return errorCode;
		}

		// Alle Attribute auf False

		setHeading("");
		setSubHeading("");
		setParts(null);
		setExplanation(false);
		setExtendedExplanation(null);
		setIndexeFarbe(null, null);

		return errorCode;

	}

	/**
	 * <p>
	 * Vergleicht einen Index mit einem Index aus einer Liste, um festzustellen, ob
	 * an der Stelle ein Tausch stattgefunden hat.
	 * </p>
	 *
	 * @param liste      - eine Liste mit indexen
	 * @param indexListe - die Stelle in der List zum Vergleichen
	 * @param index      - der index zum Vergleichen
	 * @return
	 *         <p>
	 *         true - index ist der Selbe wie in der Liste
	 *         </p>
	 *         <p>
	 *         false - index ist nicht der selbe bzw. ungueltig
	 *         </p>
	 */
	private boolean testIndex(ArrayList<Integer> liste, int indexListe, int index) {

		if (indexListe >= liste.size()) {
			return false;
		}

		if (liste.get(indexListe) == -1) {
			return false;
		}

		if (liste.get(indexListe) == index) {
			return true;
		}

		return false;
	}

	/**
	 * 
	 * <p>
	 * Hilfsmethode fuer printQueueAlgorithm. Printed die Queue in eine Tikz Datei.
	 * Wird aufgerufen waehrend der Schleife und nach einem Tausch in der Schleife.
	 * Die Ueberschriften werden entsprechend angepasst, je nachdem wo diese
	 * aufgerufen wurde, alsauch die Farben. Dies wird mit den Variabken aufsteigend
	 * und tausch angepasst.
	 * </p>
	 * 
	 * @param speicher           - Array Darstellung einer Queue
	 * @param index              - Momentaner Index der Queue
	 * @param aufsteigend        - ob die Methode in der Aufsteigenden Schleife
	 *                           aufgerufen wurde
	 * @param tausch             - ob die Methode bei einem Tausch aufgerufen wurde
	 * @param schleifenDurchlauf - anzahl der momentanen Schleifendurchlaeufe
	 * @param anfangsIndex       - Anfangs Index der Sortier Schleife
	 * @param endIndex           - End Index der Sortier Schleife
	 * @return
	 * @return
	 *         <p>
	 *         0 - funktioniert
	 *         </p>
	 *         <p>
	 *         10 - IOException bei Filwriter bzw. buffereWriter
	 *         </p>
	 *         <p>
	 *         11 - File noch nicht erstellt
	 *         </p>
	 *         <p>
	 *         12 - (sub)HeadingText fehlt bzw. ist leer
	 *         </p>
	 *         <p>
	 *         13 - (sub)HeadinText zu lang
	 *         </p>
	 *         <p>
	 *         14 - explanation too long
	 *         </p>
	 *         <p>
	 *         15 - extendedExplanation ungueltig
	 *         </p>
	 *         <p>
	 *         16 - setIndexFarben error
	 *         </p>
	 *         <p>
	 *         17 - Ueberschrift error
	 *         </p>
	 *         <p>
	 *         22 - falsche Print Methode aufgerufen
	 *         </p>
	 */
	private int printSortQueueSchleife(E[] speicher, int index, boolean aufsteigend, boolean tausch,
			int schleifenDurchlauf, int anfangsIndex, int endIndex) {

		int errorCode = 0;

		String farbe1 = "blue";
		String farbe2 = "red";
		String farbe1deutsch = "blau";
		String farbe2deutsch = "rot";
		String textExpla = "wird verglichen";
		String vergleich1 = "index imo";
		String vergleich2;
		String ueberschrift;
		String subUeberschrift = "Schleife Durchlauf Nr. " + schleifenDurchlauf;
		int index2;

		if (aufsteigend) {
			vergleich2 = "kleiner?";
			ueberschrift = "Schleife nach rechts von " + anfangsIndex + " bis " + (endIndex - 1);
			index2 = index + 1;
		} else {
			vergleich2 = "groesser?";
			ueberschrift = "Schleife nach links von " + anfangsIndex + " bis " + (endIndex + 1);
			index2 = index - 1;
		}

		if (tausch) {
			farbe1 = "red";
			farbe2 = "blue";
			farbe1deutsch = "rot";
			farbe2deutsch = "blau";
			textExpla = "getauscht";
			ueberschrift = "Tausch";
		}

		if (tausch && aufsteigend) {
			vergleich1 = "kleiner";
			vergleich2 = "groesser";
			subUeberschrift = "rot less than blau";
		} else if (tausch) {
			vergleich1 = "groesser";
			vergleich2 = "kleiner";
			subUeberschrift = "rot greater than blau";
		}

		// Farbe an Index
		if (!this.setIndexeFarbe(new int[] { index, index2 }, new String[] { farbe1, farbe2 })) {
			return 16;
		}

		// Explanation fuer Farbe an Index
		errorCode = this.setExtendedExplanation(
				new String[] { textExpla, farbe1deutsch, vergleich1, farbe2deutsch, vergleich2 });

		if (errorCode != 0) {
			return errorCode;
		}

		// Ueberschrift mit art der Schleife
		if (!this.setHeading(ueberschrift)) {
			return 17;
		}

		// UnterUeberschrift mit SchleifenDurchlauf
		if (!this.setSubHeading(subUeberschrift)) {
			return 17;
		}

		// Do Ueberschrift
		errorCode = this.doHeadings();
		if (errorCode != 0) {
			return errorCode;
		}

		// DO Explanation
		errorCode = this.doExplanations();
		if (errorCode != 0) {
			return errorCode;
		}

		// Draw Queue
		errorCode = doQueuePrintFarben(speicher);
		if (errorCode != 0) {
			return errorCode;
		}

		// Pfeil an Index
		errorCode = this.drawArrow(index, "");
		if (errorCode != 0) {
			return errorCode;
		}

		// Alle Attribute auf False

		setHeading("");
		setSubHeading("");
		setParts(null);
		setExplanation(false);
		setExtendedExplanation(null);
		setIndexeFarbe(null, null);

		return errorCode;
	}

}