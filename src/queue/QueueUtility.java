package queue;
import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * Erweitert eine Priority Queue fuer mehr Funktionalitaet. 
 * 
 * @author Oliver Kamrath
 *
 * @param <E> - zu speichernder Datentyp
 */
public class QueueUtility<E extends Comparable<E>> extends PriorityQueue<E> {
	
	/**
	 * Konstruktor des Objekts. 
	 * Speichergroesse muss groesser 0 und kleiner 100 sein, ansonsten wird eine 
	 * IllegalArgumentException geworfen.
	 * @param speicherGroesse - Groesse des Speichers der Queue
	 */
	public QueueUtility(int speicherGroesse) {
			super(speicherGroesse);
	}
	
	/**
	 * <p>
	 * Sucht ein Element in der Queue und gibt den dazugehoerigen Index aus bzw. alle 
	 * dazugehoerigen Indexe, ausgehend von dem ersten Element in der Queue.
	 * </p>
	 * 
	 * <p>
	 * Falls das Element nicht vorhanden ist wird eine NoSuchElementException
	 * geworfen. 
	 * </p>
	 * 
	 * @param e - zu suchende Element
	 * @return index[] von den gesuchten Elementen
	 * @throws NoSuchElementException - Wenn das Element nicht vorhanden ist
	 */
	public int[] search(E e) throws NoSuchElementException {
		
		ArrayList<Integer> indexSafe = new ArrayList<Integer>();
		
		for (int index = 0; index < this.size(); index++) {
			if (this.getElement(index).equals(e)) {
				 indexSafe.add(index);
			}
		}
		
		
		if(indexSafe.size() == 0) {
			throw new NoSuchElementException("Element nicht vorhanden in der Queue");
		}
		
		int[] indexes = new int[indexSafe.size()];
		for(int i = 0; i < indexes.length; i ++) {
			indexes[i] = indexSafe.get(i);
		}
		return indexes;
	}
	
	/**
	 * <p>
	 * Fuegt ein Element an der Position index in die Queue ein, ausgehend von dem
	 * ersten Element und passt alle anderen Elemente an die die neue Ordnung an.
	 * </p>
	 * <p>
	 * Falls die Queue voll ist wird 4 zurueckgegeben, falls der Index nicht in
	 * der Queue ist, also groesser als die Speichergroesse ist, wird
	 * 5 zurueckgegeben, falls der Index groesser ist als die Anzahl an Elementen
	 * in der Queue +1 wird 7 zurueckgegeben.
	 * <p>
	 * 
	 * @param e - Wert der inserted werden soll
	 * @param index - die Stelle ausgehend vom ersten Element
	 * @return 
	 * <p> 0 - funktioniert </p>
	 * <p> 1 - Speicher falsche groesse (critical) </p>
	 * <p> 2 - ZeigerAnfang falsch (critical) </p>
	 * <p> 3 - ZeigerFrei falsch (critical) </p>
	 * <p> 4 - Speicher voll </p>
	 * <p> 5 - Index groesser als der Speicher </p>
	 * <p> 6 - Index kleiner 0 </p>
	 * <p> 7 - Index ausserhalb der Queue +1 </p>
	 * 
	 */
	public int insert(E e, int index) {

		if (this.size() == this.getSpeicherSize()) {
			return 5;
		}

		if (index >= this.getSpeicherSize()) {
			return 5;
		}
		
		if (index < 0) {
			return 6;
		}

		if (index >= this.size() + 1) {
			return 7;
		}

		@SuppressWarnings("unchecked")
		E[] newSpeicher = (E[]) new Comparable[this.getSpeicherSize()];
		int zeigerFrei = 0;

		for (int i = 0; i <= this.size(); i++) {
			if (i < index) {
				newSpeicher[i] = this.getElement(i);
			} else if (i == index) {
				newSpeicher[i] = e;
			} else if (i > index) {
				newSpeicher[i] = this.getElement(i - 1);
			}
			zeigerFrei++;
		}

		if (zeigerFrei == this.getSpeicherSize()) {
			zeigerFrei = 0;
		}

		if (this.setSize(this.size() + 1)) {
			return this.setSpeicher(newSpeicher, 0, zeigerFrei);
		}

		return 0;
	}

	/**
	 * <p>
	 * Entfernt ein Element an der Position index aus der Queue, ausgehend von dem
	 * ersten Element und passt alle anderen Elemente an die neue Ordnung an.
	 * </p>
	 * <p>
	 * Falls die Queue leer ist wird 8 zurueckgegeben, falls der Index nicht in
	 * der Queue ist, also groesser als die Speichergroesse ist, wird
	 * 5 zurueckgegeben, falls der Index groesser ist als die Anzahl an Elementen
	 * in der Queue wird 7 zurueckgegeben.
	 * </p>
	 * <p>
	 * Wenn das Element erfolgreich entfernt werden konnte wird 1 zurueckgegeben.
	 * </p>
	 * 
	 * @param index - die Stelle ausgehend vom ersten Element
	 * @return
	 * <p> 0 - funktioniert </p>
	 * <p> 1 - Speicher falsche groesse (critical) </p>
	 * <p> 2 - ZeigerAnfang falsch (critical) </p>
	 * <p> 3 - ZeigerFrei falsch (critical) </p>
	 * <p> 5 - Index groesser als der Speicher </p>
	 * <p> 6 - Index kleiner 0 </p>
	 * <p> 7 - Index ausserhalb der Queue </p>
	 * <p> 8 - Queue ist leer </p>
	 */
	public int delete(int index) {
		if (this.isEmpty()) {
			return 8;
		}
		
		if( index < 0) {
			return 6;
		}

		if (index >= this.getSpeicherSize()) {
			return 5;
		}

		if (index >= this.size()) {
			return 7;
		}

		@SuppressWarnings("unchecked")
		E[] newSpeicher = (E[]) new Comparable[this.getSpeicherSize()];
		int zeigerFrei = 0;

		for (int i = 0; i < this.size(); i++) {
			if (i < index) {
				newSpeicher[i] = this.getElement(i);
				zeigerFrei++;
			} else if (i >= index) {
				newSpeicher[i] = this.getElement(i + 1);
				zeigerFrei++;
			}
		}

		if (zeigerFrei == this.getSpeicherSize()) {
			zeigerFrei = 0;
		}

		if (this.setSize(this.size() - 1)) {
			return this.setSpeicher(newSpeicher, 0, zeigerFrei);
		}

		return 1;
	}

	/**
	 * <p>
	 * Aendert ein Element in der Queue zu newe and der Stelle index, ausgehend von
	 * dem ersten Element.
	 * </p>
	 * <p>
	 * Falls die Queue leer ist wird 8 zurueckgegeben, Falls der Index nicht in
	 * der Queue ist, also groesser als die Speichergroesse ist, wird
	 * 5 zurueckgegeben, falls der Index groesser ist als die Anzahl an Elementen
	 * in der Queue wird 7 zurueckgegeben
	 * </p>
	 * @param newe - neuer Wert fuer das Element
	 * @param index - Ort ausgehend vom ersten Element
	 * @return 
	 * <p> 0 - funktioniert </p>
	 * <p> 1 - Speicher falsche groesse (critical) </p>
	 * <p> 2 - ZeigerAnfang falsch (critical) </p>
	 * <p> 3 - ZeigerFrei falsch (critical) </p>
	 * <p> 5 - Index groesser als der Speicher </p>
	 * <p> 6 - Index kleiner 0 </p>
	 * <p> 7 - Index ausserhalb der Queue </p>
	 * <p> 8 - Queue ist leer </p>
	 */
	public int update(E newe, int index) {

		if (this.isEmpty()) {
			return 8;
		}

		if (index >= this.getSpeicherSize()) {
			return 5;
		}
		
		if ( index < 0) {
			return 6;
		}

		if (index >= this.size()) {
			return 7;
		}

		@SuppressWarnings("unchecked")
		E[] newSpeicher = (E[]) new Comparable[this.getSpeicherSize()];
		int zeigerFrei = 0;

		for (int i = 0; i < this.size(); i++) {
			if (i < index) {
				newSpeicher[i] = this.getElement(i);
			} else if (i > index) {
				newSpeicher[i] = this.getElement(i);
			} else if (i == index) {
				newSpeicher[i] = newe;
			}
			zeigerFrei++;
		}

		if (zeigerFrei == this.getSpeicherSize()) {
			zeigerFrei = 0;
		}

		return this.setSpeicher(newSpeicher, 0, zeigerFrei);
	}

	/**
	 * <p>
	 * Gibt die Queue als Array zurueck, wobei die groe�e des Arrays gleich der size der Queue ist. 
	 * Das erste Element ist an Platzt 0. 
	 * </p>
	 * @return ein Array das die Queue darstellt
	 * <p> null wenn diese leer ist </p>
	 */
	public E[] toArray() {
		if(this.isEmpty()) {
			return null;
		}
		
		@SuppressWarnings("unchecked")
		E[] newSpeicher = (E[]) new Comparable[this.size()];
		for (int i = 0; i < this.size(); i++) {
				newSpeicher[i] = this.getElement(i);
		}
		
		return newSpeicher;
	}
}
