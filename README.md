Softwaretechnik-Projekt *"Visualisierung von Datenstrukturen"* bei *Steffen Vaupel* im Wintersemester 20/21 an der Technischen Hochschule Mittelhessen in Giessen

Dieses Projekt wird durchgeführt von:
- Loic Cesar Boubeji Tiangueu
- Oliver Kamrath
- Walid Said
- Sthemi Macnel Baloka Yimi
- Patrick Hay

In diesem Projekt sollen die Datenstrukturen **Stack** , **Queue** , **List** implementiert und die Arbeit mit diesen Datenstrukturen visualisiert werden.

